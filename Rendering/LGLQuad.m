//
//  LGLRenderer.m
//  Writeability
//
//  Created by Ryan on 12/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLQuad.h"

#import "LGLTexture_Private.h"

#import "LGLProgram.h"

#import <Utilities/UIColor+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLQuad
#pragma mark -
//*********************************************************************************************************************//




@interface LGLQuad ()

@property (nonatomic, readwrite , assign) CGRect        coordRect;
@property (nonatomic, readwrite , assign) CGRect        vertexRect;

@property (nonatomic, readwrite , strong) NSData        *transformData;
@property (nonatomic, readwrite , strong) NSData        *colorData;

@property (nonatomic, readwrite , assign) LGLIMatrix2x2 viewport;

@end

@implementation LGLQuad

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFormat:(NSString *)format __ILLEGALMETHOD__;

- (instancetype)initWithTexture:(LGLTexture *)texture
{
    return [self initWithTexture:texture inverted:NO];
}

- (instancetype)initWithTexture:(LGLTexture *)texture region:(CGRect)region
{
    return [self initWithTexture:texture region:region inverted:NO];
}

- (instancetype)initWithTexture:(LGLTexture *)texture region:(CGRect)region vectorized:(BOOL)vectorized
{
    return [self initWithTexture:texture region:region vectorized:vectorized inverted:NO];
}

- (instancetype)initWithTexture:(LGLTexture *)texture inverted:(BOOL)inverted
{
    return
    [self
     initWithTexture:texture
     region         :((CGRect) {
        .origin = { 0 },
        .size   = {
            .width  = texture.width,
            .height = texture.height
        }
    })
     inverted       :inverted];
}

- (instancetype)initWithTexture:(LGLTexture *)texture region:(CGRect)region inverted:(BOOL)inverted
{
    return [self initWithTexture:texture region:region vectorized:NO inverted:inverted];
}

- (instancetype)
initWithTexture :(LGLTexture *  )texture
region          :(CGRect        )region
vectorized      :(BOOL          )vectorized
inverted        :(BOOL          )inverted
{
    DBGParameterAssert((texture != nil) && !CGRectIsEmpty(region));

    if ((self = [super initWithFormat:@"aPosition:p2f, aTexCoords:t2f"])) {
        _texture    = texture;
        _region     = region;
        _target     = region;
        _vectorized = vectorized;
        _inverted   = inverted;

        [self setTransform:LGLMatrix4x4Identity];
        [self setColor:nil];
    }
    
    return self;
}


#pragma mark - LGLRenderable
//*********************************************************************************************************************//

- (void)render
{
    [self renderAtPoint:_target.origin];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setTransform:(LGLMatrix4x4)transform
{
    {_transform = transform;}

    _transformData = Datafy(transform);
}

- (void)setColor:(UIColor *)color
{
    {_color = color;}

    LGLVector4 components;

    [_color?:[UIColor blackColor]
     getComponents:components.v];

    _colorData = Datafy(components);
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)renderAtPoint:(CGPoint)point
{
    CGRect target = _target;
    target.origin = point;

    [self renderInRect:target];
}

- (void)renderInRect:(CGRect)rect
{
    LGLProgram *program
    =
    ([self program]?:
     [LGL objectPool]
     [!(_vectorized)? (_color)?
      kLGLProgramColoredTexKey:
      kLGLProgramDefaultTexKey:
      kLGLProgramSDFieldTexKey]);

    [program use];

    if ([_texture prepareForRendering]) {
        [program setData:_transformData forUniformWithID:@"uTransform"];

        if ((_vectorized) || (_color)) {
            [program setData:_colorData forUniformWithID:@"uColor"];
        }

        [self updateVerticesWithTarget:rect];
        [self blit];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)updateVerticesWithTarget:(CGRect)target
{
    DBGParameterAssert(!CGRectIsEmpty(target));

    if (!CGRectEqualToRect(_coordRect, _region) ||
        !CGRectEqualToRect(_vertexRect, target) ||
        !LGLIMatrix2x2AllEqualToIMatrix2x2
        ([self viewport], [LGL viewport]))
    {
        _vertexRect = target;
        _viewport   = [LGL viewport];

        GLuint          surfaceWidth    = LFMDivf(_viewport.w, _texture.scale);
        GLuint          surfaceHeight   = LFMDivf(_viewport.h, _texture.scale);

        LGLMatrix4x2    texVertices     = LGLTexVertices((LGLMatrix2x2)
                                                         target,
                                                         surfaceWidth,
                                                         surfaceHeight);

        LGLMatrix4x2    texCoords       = (!(_inverted)?
                                           LGLTexCoords
                                           ((LGLMatrix2x2)_region, _texture.width, _texture.height):
                                           LGLTexCoordsInverted
                                           ((LGLMatrix2x2)_region, _texture.width, _texture.height));

        LGLMatrix4x4    vertices        = LGLInterleaveVertices(texVertices, texCoords);

        [self setVertexData:Datafy(vertices)];
    }
}

@end
