//
//  LGLShaderAPositionUColorTextureSDF.h
//  Writeability
//
//  Created by Ryan on 1/16/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


@$_quote
(
 varying lowp vec2 vTexCoords;

 @if defined VERTEX_SHADER;

 uniform    mat4 uTransform;
 attribute  vec4 aPosition;
 attribute  vec2 aTexCoords;

 void main(void) {
     gl_Position    = uTransform * aPosition;
     vTexCoords     = aTexCoords;
 }

 @elif defined FRAGMENT_SHADER;

 @extension $GL_OES_standard_derivatives : enable;

 precision lowp float;

 uniform sampler2D  uSampler;
 uniform vec4       uColor;

 void main(void) {
     float alpha    = texture2D(uSampler, vTexCoords).a;
     float width    = length(vec2(dFdx(alpha), dFdy(alpha)));

     gl_FragColor   = uColor * smoothstep(.5- width, .5+ width, alpha);
 }
 
 @endif;
 );
