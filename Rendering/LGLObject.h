//
//  LGLObject.h
//  Writeability
//
//  Created by Ryan on 12/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//



#import "LGL.h"


@interface LGLObject : NSObject

@property (atomic, readwrite, assign) void *context;

@end
