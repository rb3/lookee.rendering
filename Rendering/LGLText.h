//
//  LGLText.h
//  Writeability
//
//  Created by Ryan on 1/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLObject.h"
#import "LGLRenderable.h"



@interface LGLText : LGLObject <LGLRenderable>

@property (nonatomic, readwrite , assign) CGRect                frame;
@property (nonatomic, readwrite , strong) NSAttributedString    *attributedString;


- (instancetype)initWithFrame:(CGRect)frame;

@end
