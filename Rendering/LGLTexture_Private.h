//
//  LGLTexture_Private.h
//  Writeability
//
//  Created by Ryan on 1/1/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLTexture.h"


@interface LGLTexture ()

- (BOOL)initialize;
- (BOOL)prepareForRendering;

- (void)setAsBacking;

@end
