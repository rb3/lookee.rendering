//
//  LGLFont.m
//  Writeability
//
//  Created by Ryan on 1/23/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import "LGLFont.h"

#import "LGLAtlas.h"
#import "LGLQuad.h"

#import <Utilities/LBitmap.h>

#import <CoreText/CoreText.h>

#import <map>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Types
#pragma mark -
//*********************************************************************************************************************//




typedef struct LGLFRegion
{
    NSUInteger  index;
    CGRect      frame;
} LGLFRegion;




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLFont
#pragma mark -
//*********************************************************************************************************************//



@interface LGLFont ()

@property (nonatomic, readonly, strong) NSMutableArray *atlases;

@end

@implementation LGLFont

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    std::map< CGGlyph, LGLFRegion >                     _vectorGlyphs;
    std::map< CGGlyph, std::map<CGFloat, LGLFRegion> >  _rasterGlyphs;

    CTFontRef       __vectorFont;

    NSMutableData   *__vectorCache;
    NSMutableData   *__bitmapCache;
}


#pragma mark - Static Objects
//*********************************************************************************************************************//

static const NSUInteger kLGLFontAtlasWidth  = 512;
static const NSUInteger kLGLFontAtlasHeight = 512;

static const NSUInteger kLGLFontVectorSize  = 32;
static const NSUInteger kLGLFontMarginSize  = 2;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithFontName:(NSString *)name
{
    if ((self = [super init])) {
        _name       = name.copy;

        _atlases    = [NSMutableArray new];
    }

    return self;
}

- (void)dealloc
{
    if (__vectorFont) {
        CFRelease(__vectorFont);
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (LGLQuad *)rendererForGlyph:(CGGlyph)glyph pointSize:(float)size
{
#if !DEBUG || !DEBUG_DISABLE_BITMAP_FONTS
    if (isgreater(LGL.scale, 1.0)) {
        if (islessequal(size, kLGLFontVectorSize*0.5)) {
            return [self rasterRendererForGlyph:glyph pointSize:size];
        }
    } else {
        if (isless(size, kLGLFontVectorSize*1.0)) {
            return [self rasterRendererForGlyph:glyph pointSize:size];
        }
    }
#endif

    return [self vectorRendererForGlyph:glyph pointSize:size];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LGLQuad *)rasterRendererForGlyph:(CGGlyph)glyph pointSize:(CGFloat)size
{
    LGLFRegion &region = _rasterGlyphs[glyph][size];

    if (CGRectIsEmpty(region.frame) && !CGRectIsNull(region.frame)) {
        CTFontRef   font = CTFontCreateWithName((__bridge CFStringRef)(_name), size, &CGAffineTransformIdentity);
        CGRect      rect ; CTFontGetBoundingRectsForGlyphs(font, kCTFontDefaultOrientation, &glyph, &rect, 1);

        if (!CGRectIsEmpty(rect)) {
            NSUInteger width  = (LFMCeilf((rect.size.width  + kLGLFontMarginSize *2.) /8. +1.) *8.);
            NSUInteger height = (LFMCeilf((rect.size.height + kLGLFontMarginSize *2.) /8. +1.) *8.);

            region
            =
            [self reserveAtlasRegion:
             ^CGRect(LGLAtlas *atlas) {
                return
                [atlas
                 renderSprite:^{
                     CGContextRef context = UIGraphicsGetCurrentContext();

                     CGContextTranslateCTM((context),
                                           (-rect.origin.x+kLGLFontMarginSize),
                                           (-rect.origin.y-kLGLFontMarginSize
                                            -rect.size.height
                                            +height));

                     CGContextSetTextDrawingMode(context, kCGTextFill);
                     CGContextSetGrayFillColor(context, 1., 1.);

                     CTFontDrawGlyphs(font, &glyph, &CGPointZero, 1, context);
                 } withWidth:width height:height];
            }];
            
            CFRelease(font);
        } else {
            region.frame = CGRectNull;
        }
    }

    if (!CGRectIsEmpty(region.frame)) {
        return [[LGLQuad alloc]
                initWithTexture :_atlases[region.index]
                region          :region.frame];
    }

    return nil;
}

- (LGLQuad *)vectorRendererForGlyph:(CGGlyph)glyph pointSize:(CGFloat)size
{
    LGLFRegion &region = _vectorGlyphs[glyph];

    if (CGRectIsEmpty(region.frame) && !CGRectIsNull(region.frame)) {
        if (!(__vectorFont)) {
            __vectorFont  = CTFontCreateWithName((__bridge CFStringRef)(_name),
                                                 kLGLFontVectorSize*[LGL scale],
                                                 &CGAffineTransformIdentity);

            CGRect          bounds      = CTFontGetBoundingBox(__vectorFont);
            NSUInteger      width       = (LFMCeilf((bounds.size.width  + kLGLFontMarginSize *2.) /8. +1.) *8.);
            NSUInteger      height      = (LFMCeilf((bounds.size.height + kLGLFontMarginSize *2.) /8. +1.) *8.);

            __vectorCache = [NSMutableData dataWithLength:width * height * sizeof(GLbyte)];
            __bitmapCache = [NSMutableData dataWithLength:width * height * sizeof(double)];
        }

        CGRect rect; CTFontGetBoundingRectsForGlyphs(__vectorFont, kCTFontDefaultOrientation, &glyph, &rect, 1);

        if (!CGRectIsEmpty(rect)) {
            NSUInteger      width       = (LFMCeilf((rect.size.width  + kLGLFontMarginSize *2.) /8. +1.) *8.);
            NSUInteger      height      = (LFMCeilf((rect.size.height + kLGLFontMarginSize *2.) /8. +1.) *8.);

            CGContextRef    context     = CGBitmapContextCreate(__vectorCache.mutableBytes,
                                                                width,
                                                                height,
                                                                8,
                                                                width,
                                                                NULL,
                                                                kCGImageAlphaOnly);
            
            CGContextClearRect(context, ((CGRect){{0}, static_cast<CGFloat>(width), static_cast<CGFloat>(height)}));

            CGContextTranslateCTM(context, 0., height);
            CGContextScaleCTM(context, 1., -1.);

            CGContextTranslateCTM((context),
                                  (-rect.origin.x+kLGLFontMarginSize),
                                  (-rect.origin.y-kLGLFontMarginSize
                                   -rect.size.height
                                   +height));

            CGContextSetTextDrawingMode(context, kCGTextFill);
            CGContextSetGrayFillColor(context, 1., 1.);

            CTFontDrawGlyphs(__vectorFont, &glyph, &CGPointZero, 1, context);

            CGContextRelease(context);

            LBitmapConvertFromBytes(__vectorCache.bytes, width, height, __bitmapCache.mutableBytes);
            LBitmapDistanceField(__bitmapCache.bytes, width, height, __bitmapCache.mutableBytes);
            LBitmapConvertToBytes(__bitmapCache.bytes, width, height, __vectorCache.mutableBytes);

            region
            =
            [self reserveAtlasRegion:
             ^CGRect(LGLAtlas *atlas) {
                return
                [atlas addBitmap:
                 [__vectorCache
                  subdataWithRange:
                  ((NSRange) {
                     .location  = 0,
                     .length    = height * width
                 })] withRows:height stride:width];
             }];
        } else {
            region.frame = CGRectNull;
        }
    }

    if (!CGRectIsEmpty(region.frame)) {
        CGFloat factor      = LFMDivf(size, kLGLFontVectorSize);
        CGRect  target      = CGRectScale(region.frame, factor);

        LGLQuad *quad       = [[LGLQuad alloc]
                               initWithTexture  :_atlases[region.index]
                               region           :region.frame
                               vectorized       :YES];

        [quad setTarget:target];
        
        return quad;
    }

    return nil;
}

- (LGLFRegion)reserveAtlasRegion:(CGRect(^)(LGLAtlas *))block
{
    LGLFRegion region = {0};

    $for (LGLAtlas *atlas in _atlases) {
        if (!CGRectIsEmpty(region.frame = block(atlas))) {
            break;
        }

        ++ region.index;
    } else {
        LGLAtlas *atlas  = [[LGLAtlas alloc]
                            initWithWidth   :kLGLFontAtlasWidth
                            height          :kLGLFontAtlasHeight
                            depth           :1
                            scale           :[LGL scale]];

        region.frame = block(atlas);

        [_atlases addObject:atlas];
    }

    return region;
}

@end
