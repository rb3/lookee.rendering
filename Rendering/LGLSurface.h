//
//  LGLViewport.h
//  Writeability
//
//  Created by Ryan on 8/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "LGLRenderable.h"



@protocol LGLSurfaceDelegate;



@interface LGLSurface : UIView

@property (nonatomic, readwrite , weak  ) id                            <LGLSurfaceDelegate>delegate;

@property (nonatomic, readonly  , assign) NSUInteger                    width;
@property (nonatomic, readonly  , assign) NSUInteger                    height;
@property (nonatomic, readonly  , assign) float                         scale;

@property (nonatomic, readonly	, assign) NSUInteger                    samples;

@property (nonatomic, readwrite	, weak, setter = connectToSurface:)     LGLSurface *connectedSurface;


- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
samples         :(NSUInteger)samples;


- (void)batchRenderOperations:(void(^)(void))block;
- (void)queueRenderOperation:(void(^)(void))block;

- (void)clear;

@end


@protocol LGLSurfaceDelegate <NSObject>
@required

- (void)surface:(LGLSurface *)surface renderInRect:(CGRect)rect;

@end
