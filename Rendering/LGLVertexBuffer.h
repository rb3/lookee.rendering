//
//  LGLVertexBuffer.h
//  Writeability
//
//  Created by Ryan on 1/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLObject.h"



@interface LGLVertexBuffer : LGLObject

@property (nonatomic, readonly  , strong) NSString    *format;

@property (nonatomic, readonly  , assign) NSUInteger  length;

@property (nonatomic, readwrite , strong) NSData      *vertexData;


- (instancetype)initWithFormat:(NSString *)format;

- (void)appendVertexData:(NSData *)data;
- (void)packVertices;

/**
 * This method must be called
 * from within a LGL queue block.
 */
- (void)blit;

@end
