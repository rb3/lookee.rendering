//
//  LGLImage.m
//  Writeability
//
//  Created by Ryan on 10/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLTexture_Private.h"

#import "LGLProgram.h"

#import <Utilities/UIColor+Utilities.h>

#import <OpenGLES/ES2/glext.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLImage
#pragma mark -
// Partially derived from the GPUImage Project @ https://github.com/BradLarson/GPUImage
//*********************************************************************************************************************//




@interface LGLTexture ()

@property (nonatomic, readonly  , assign) CVOpenGLESTextureCacheRef textureCache;
@property (nonatomic, readwrite , assign) CVOpenGLESTextureRef		texture;
@property (nonatomic, readonly  , assign) CVPixelBufferRef			pixelBuffer;

@end

@implementation LGLTexture

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    NSMutableData   *_data;

    BOOL            __initialized;

    BOOL            __locked;
    CVOptionFlags   __lockMode;
}


#pragma mark - Static Objects
//*********************************************************************************************************************//

static const GLuint kTextureDefaultDepth = 4;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
{
    return [self initWithWidth:width height:height scale:1.];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
{
    return [self
            initWithWidth   :width
            height          :height
            scale           :scale
            data            :nil];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
{
    return [self
            initWithWidth   :width
            height          :height
            depth           :depth
            scale           :scale
            data            :nil];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
data            :(NSData *  )data
{
    return [self
            initWithWidth   :width
            height          :height
            depth           :kTextureDefaultDepth
            scale           :scale
            data            :data];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
data            :(NSData *  )data
{
    DBGParameterAssert((width  != 0) &&
                        (height != 0) &&
                        (depth  <= 4));

	if ((self = [super init])) {
		_width          = width;
		_height         = height;
        _depth          = depth;
        _scale          = scale;

        _rows           = (height * scale);
        _columns        = (width  * scale);
        _bytesPerRow    = (_columns * _depth);

        _data           = [data mutableCopy];
        _data.length    = (_rows) * (_bytesPerRow);
	}
    
	return self;
}

- (void)dealloc
{
    CVOpenGLESTextureRef        texture         = _texture;
    CVOpenGLESTextureCacheRef   textureCache    = _textureCache;
    CVPixelBufferRef            pixelBuffer     = _pixelBuffer;

	[LGL queueAsyncOperation:^{
        GLenum target = CVOpenGLESTextureGetTarget(texture);

        if ([LGL textureForTarget:target]
            == CVOpenGLESTextureGetName(texture)) {
            [LGL unbindTextureAtTarget:target];
        }

        if (textureCache) {
            CVOpenGLESTextureCacheFlush(textureCache, 0);
			CFRelease(textureCache);
        }

        if (pixelBuffer) {
            CVPixelBufferRelease(pixelBuffer);
        }

		if (texture) {
			CFRelease(texture);
		}
	}];

	DBGMessage(@"deallocated %@", self);
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (CGContextRef)mappedContext
{
    CGContextRef __block context = NULL;

    [LGL queueSyncOperation:^{
        if ([self initialize]) {
            [self lockData];

            if ((context = LGLTextureBitmapContext(_pixelBuffer, _depth))) {
                CGContextScaleCTM(context, _scale, _scale);
            }
        }
    }];

    return context;
}

- (UIImage *)imageWithScale:(float)scale
{
	UIImage __block     *image		= nil;

    [LGL queueSyncOperation:^{
        if ([self initialize]) {
            [self unlockData];

            CGContextRef    context     = [self mappedContext];
            CGImageRef      imageRef	= CGBitmapContextCreateImage(context);

            image = [UIImage
                     imageWithCGImage   :imageRef
                     scale              :scale
                     orientation        :UIImageOrientationDownMirrored];

            CGImageRelease(imageRef);
            CGContextRelease(context);
        }
    }];

	return image;
}

- (UIColor *)colorAtPoint:(CGPoint)point
{
    UIColor __block     *color      = nil;

    [LGL queueSyncOperation:^{
        if ([self initialize]) {
            [self lockDataReadonly];

            GLubyte     *pixels     = CVPixelBufferGetBaseAddress(_pixelBuffer);
            GLsizeiptr  rows        = CVPixelBufferGetHeight(_pixelBuffer);
            GLsizeiptr  columns     = CVPixelBufferGetWidth(_pixelBuffer);
            GLsizeiptr  bytesPerRow = CVPixelBufferGetBytesPerRow(_pixelBuffer);
            GLsizeiptr  byteCount   = (rows * bytesPerRow);

            if (pixels) {
                GLuint	byteIndex   = ((LFMFloorf(-point.y + rows)
                                        * bytesPerRow) +
                                       (LFMFloorf(+point.x)
                                        * (bytesPerRow / columns)));

                if ((byteIndex +3) < byteCount) {
                    CGFloat blue  = (pixels[byteIndex +0] /255.);
                    CGFloat green = (pixels[byteIndex +1] /255.);
                    CGFloat red   = (pixels[byteIndex +2] /255.);
                    CGFloat alpha = (pixels[byteIndex +3] /255.);

                    color = [UIColor
                             colorWithRed   :red
                             green          :green
                             blue           :blue
                             alpha          :alpha];
                }
            }
        }
    }];
    
	return color;
}

- (void)setColor:(UIColor *)color atPoint:(CGPoint)point;
{
    [LGL queueAsyncOperation:^{
        if ([self initialize]) {
            [self lockData];

            GLubyte     *pixels     = CVPixelBufferGetBaseAddress(_pixelBuffer);
            GLsizeiptr  rows        = CVPixelBufferGetHeight(_pixelBuffer);
            GLsizeiptr  columns     = CVPixelBufferGetWidth(_pixelBuffer);
            GLsizeiptr  bytesPerRow = CVPixelBufferGetBytesPerRow(_pixelBuffer);

            if (pixels) {
                GLuint	byteIndex   = ((LFMNintf(-point.y + rows)
                                        * bytesPerRow) +
                                       (LFMNintf(+point.x)
                                        * (bytesPerRow / columns)));

                LGLVector4 rgba;
                [color getComponents:rgba.v];

                pixels[byteIndex +0] = (GLubyte)(rgba.r *255.);
                pixels[byteIndex +1] = (GLubyte)(rgba.g *255.);
                pixels[byteIndex +2] = (GLubyte)(rgba.b *255.);
                pixels[byteIndex +3] = (GLubyte)(rgba.a *255.);
            }
#if DEBUG
            [self lockDataReadonly];
#endif
        }
    }];
}

- (void)
setBitmap   :(NSData *  )bitmap
withRows    :(NSUInteger)rows
stride      :(NSUInteger)stride
atRow       :(NSUInteger)row
column      :(NSUInteger)column
{
    [LGL queueAsyncOperation:^{
        if ([self initialize]) {
            [self lockData];

            NSUInteger      width       = DivF3(bitmap.length, self.depth, rows);

            GLubyte         *pixels     = CVPixelBufferGetBaseAddress(_pixelBuffer);
            GLsizeiptr      columns     = CVPixelBufferGetWidth(_pixelBuffer);
            
            GLubyte const   *bytes      = [bitmap bytes];

            for (NSUInteger offset = 0; offset < rows; ++ offset) {
                NSUInteger position = (row + offset);

                memcpy((pixels + (_depth) * (position * columns + column)),
                       (bytes  + (offset  * stride)),
                       (width  * (_depth)));
            }
#if DEBUG
            [self lockDataReadonly];
#endif
        }
    }];
}

- (void)drawBitmap:(void(^)(void))block
{
    DBGParameterAssert(block != NULL);

    [LGL queueAsyncOperation:^{
        if ([self initialize]) {
            CGContextRef context = [self mappedContext];
            
            UIGraphicsPushContext(context);
            block();
            UIGraphicsPopContext();

            CGContextRelease(context);
#if DEBUG
            [self lockDataReadonly];
#endif
        }
    }];
}

- (void)clear
{
    [self drawBitmap:^{
        CGContextClearRect
        (UIGraphicsGetCurrentContext(),
         ((CGRect){
            .origin = (CGPointZero),
            .size   = {_width, _height}
        }));
    }];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)bind
{
    [LGL activateTextureUnit:GL_TEXTURE0];
    [LGL bindTexture:CVOpenGLESTextureGetName(_texture)
            toTarget:CVOpenGLESTextureGetTarget(_texture)];
}

- (BOOL)lockData
{
    if ((__locked == NO) || (__lockMode != 0)) {
        if ([self unlockData]) {
            CVReturn result = CVPixelBufferLockBaseAddress(_pixelBuffer, 0);

            if (result == kCVReturnSuccess) {
                __locked    = YES;
                __lockMode  = 0;
            } else {
                DBGWarning(@"Failed to lock pixel buffer (%d)", result);

                return NO;
            }
        } else {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)lockDataReadonly
{
    if ((__locked == NO) || (__lockMode == 0)) {
        if ([self unlockData]) {
            CVReturn result = CVPixelBufferLockBaseAddress(_pixelBuffer, kCVPixelBufferLock_ReadOnly);

            if (result == kCVReturnSuccess) {
                __locked    = YES;
                __lockMode  = kCVPixelBufferLock_ReadOnly;
            } else {
                DBGWarning(@"Failed to lock pixel buffer (%d)", result);
                
                return NO;
            }
        } else {
            return NO;
        }
    }

    return YES;
}

- (BOOL)unlockData
{
    if (__locked == YES) {
        CVReturn result = CVPixelBufferUnlockBaseAddress(_pixelBuffer, __lockMode);

        if (result == kCVReturnSuccess) {
            __locked = NO;
        } else {
            DBGWarning(@"Failed to unlock pixel buffer (%d)", result);

            return NO;
        }
    }
    
    return YES;
}


#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (BOOL)initialize
{
    if (__initialized) {
        return YES;
    }

	CVReturn result = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault,
												   NULL,
												   [EAGLContext currentContext],
												   NULL,
												   &_textureCache);

	if (result == kCVReturnSuccess) {
        OSType  pixelFormatType;
        GLint   internalFormat;
        GLint   externalFormat;

        switch (_depth) {
            case 1: {
                pixelFormatType = kCVPixelFormatType_OneComponent8;
                internalFormat  = GL_ALPHA;
                externalFormat  = GL_ALPHA;
            } break;
            case 2: {
                pixelFormatType = kCVPixelFormatType_TwoComponent8;
                internalFormat  = GL_RG_EXT;
                externalFormat  = GL_RG_EXT;
            } break;
            case 3: {
                pixelFormatType = kCVPixelFormatType_24RGB;
                internalFormat  = GL_RGB8_OES;
                externalFormat  = GL_RGB8_OES;
            } break;
            default: {
                pixelFormatType = kCVPixelFormatType_32BGRA;
                internalFormat  = GL_RGBA;
                externalFormat  = GL_BGRA;
            } break;
        }

		NSDictionary *attributes = (@{ Objectify(kCVPixelBufferIOSurfacePropertiesKey): @{ } });

        result = CVPixelBufferCreate(kCFAllocatorDefault,
                                     _columns,
                                     _rows,
                                     pixelFormatType,
                                     (__bridge CFDictionaryRef)(attributes),
                                     &_pixelBuffer);

		if (result == kCVReturnSuccess) {
            if (_data) {
                [self lockData];
                memcpy(CVPixelBufferGetBaseAddress(_pixelBuffer), (_data.bytes), (_data.length));
                [self unlockData];
            }

            [self lockDataReadonly];

            GLsizeiptr  rows    = CVPixelBufferGetHeight(_pixelBuffer);
            GLsizeiptr  columns = CVPixelBufferGetWidth(_pixelBuffer);

            CVReturn    result  = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                               _textureCache,
                                                                               _pixelBuffer,
                                                                               NULL,
                                                                               GL_TEXTURE_2D,
                                                                               internalFormat,
                                                                               columns,
                                                                               rows,
                                                                               externalFormat,
                                                                               GL_UNSIGNED_BYTE,
                                                                               0,
                                                                               &_texture);
            if (result == kCVReturnSuccess) {
                [self bind];

                GLenum target = CVOpenGLESTextureGetTarget(_texture);
                glTexParameterf(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameterf(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            } else {
                DBGWarning(@"Failed to create texture (%d)", result);
            }
        }else {
			DBGWarning(@"Failed to create pixel buffer (%d)", result);
		}
	} else {
		DBGWarning(@"Failed to create texture cache (%d)", result);
	}

    if (result == kCVReturnSuccess) {
        {__initialized = YES;}

        return YES;
    }

	return NO;
}

- (BOOL)prepareForRendering
{
    if ([self initialize] &&
        [self lockDataReadonly]) {
        [self bind];

        return YES;
	}

    return NO;
}

- (void)setAsBacking
{
    glFramebufferTexture2D(GL_FRAMEBUFFER,
                           GL_COLOR_ATTACHMENT0,
                           CVOpenGLESTextureGetTarget(_texture),
                           CVOpenGLESTextureGetName(_texture),
                           0);
}


#pragma mark - Protected Functions
//*********************************************************************************************************************//

static inline CGContextRef LGLTextureBitmapContext(CVPixelBufferRef pixelBuffer, NSUInteger depth)
{
    GLubyte             *pixels     = CVPixelBufferGetBaseAddress(pixelBuffer);

    if (pixels) {
        GLsizeiptr      rows        = CVPixelBufferGetHeight(pixelBuffer);
        GLsizeiptr      columns     = CVPixelBufferGetWidth(pixelBuffer);
        GLsizeiptr      bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);

        CGBitmapInfo    info        = 0;
        CGColorSpaceRef colorspace  = NULL;

        switch (depth) {
            case 1: {
                info = (CGBitmapInfo)(kCGImageAlphaOnly);
            } break;
            case 2: {
                /*
                 * TODO: Create Core Graphics colorspace with two components
                 */
                DBGAssert(NO);
            } break;
            case 3: {
                info        = (kCGBitmapByteOrder32Little | kCGImageAlphaNone);
                colorspace  = CGColorSpaceCreateDeviceRGB();
            } break;
            default: {
                info        = (kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
                colorspace  = CGColorSpaceCreateDeviceRGB();
            } break;
        }

        CGContextRef context = CGBitmapContextCreate(pixels,
                                                     columns,
                                                     rows,
                                                     8,
                                                     bytesPerRow,
                                                     colorspace,
                                                     info);

        if (colorspace) {
            CGColorSpaceRelease(colorspace);
        }

        return context;
    }
    
    return nil;
}

@end
