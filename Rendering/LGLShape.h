//
//  LGLShape.h
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLVertexBuffer.h"
#import "LGLRenderable.h"



@class LGLProgram;



@interface LGLShape : LGLVertexBuffer <LGLRenderable>

@property (nonatomic, readwrite , strong) LGLProgram    *program;
@property (nonatomic, readwrite , assign) LGLMatrix4x4  transform;
@property (nonatomic, readwrite , strong) UIColor       *color;


- (instancetype)initWithTransform:(LGLMatrix4x4)transform;

@end
