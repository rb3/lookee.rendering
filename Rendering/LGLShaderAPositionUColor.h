//
//  LGLShaderAPositionUColor.h
//  Writeability
//
//  Created by Ryan on 1/14/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LMacros.h>


@$_quote
(
 @if defined VERTEX_SHADER;

 uniform    mat4 uTransform;
 attribute  vec4 aPosition;

 void main(void) {
     gl_Position = uTransform * aPosition;
 }

 @elif defined FRAGMENT_SHADER;

 precision lowp float;

 uniform vec4 uColor;

 void main(void) {
     gl_FragColor = uColor;
 }

 @endif;
 );
