//
//  LGLFont.h
//  Writeability
//
//  Created by Ryan on 1/23/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLObject.h"



@class LGLQuad;



@interface LGLFont : LGLObject

@property (nonatomic, readonly, strong) NSString *name;


- (instancetype)initWithFontName:(NSString *)name;

- (LGLQuad *)rendererForGlyph:(CGGlyph)glyph pointSize:(float)pointSize;

@end
