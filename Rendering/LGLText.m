//
//  LGLText.m
//  Writeability
//
//  Created by Ryan on 1/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLText.h"

#import "LGLQuad.h"

#import "LGLFont.h"

#import <Utilities/NSAttributedString+Attributes.h>

#import <CoreText/CoreText.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLText
#pragma mark -
//*********************************************************************************************************************//




@interface LGLText ()

@property (nonatomic, readwrite, assign) CGPathRef  textPath;
@property (nonatomic, readwrite, strong) NSArray    *renderers;

@end

@implementation LGLText

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const CGFloat kLGLTextHOffset = 4.;
static const CGFloat kLGLTextRMargin = 7.;
static const CGFloat kLGLTextVOffset = 8.;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super init])) {
        [self setFrame:frame];
    }

    return self;
}

- (void)dealloc
{
    if (_textPath) {
        CGPathRelease(_textPath);
    }
}


#pragma mark - LGLRenderable
//*********************************************************************************************************************//

- (void)render
{
    if (_attributedString) {
        if (!_renderers) {
            _renderers = [self renderersForAttributedString:_attributedString];
        }

        for (LGLQuad *renderer in _renderers) {
            [renderer render];
        }
    }
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setFrame:(CGRect)frame
{
    DBGParameterAssert(!CGRectIsEmpty(frame));

    if (!CGRectEqualToRect(_frame, frame)) {
        {_frame = frame;}

        frame.size.width -= kLGLTextRMargin;
        CGPathRef path    = CGPathCreateWithRect(((CGRect){{0}, frame.size}), NULL);

        if (_textPath) {
            CGPathRelease(_textPath);
        }

        {_textPath      = path;}
        {_renderers     = nil;}
    }
}

- (void)setAttributedString:(NSAttributedString *)attributedString
{
    if (![_attributedString isEqualToAttributedString:attributedString]) {
        {_attributedString  = attributedString.copy;}
        {_renderers         = nil;}
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LGLFont *)fontForTextRun:(CTRunRef)run
{
    CTFontRef   runFont     = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
    NSString    *name       = CFBridgingRelease(CTFontCopyPostScriptName(runFont));

    LGLFont     *font       = [LGL objectPool][name];

    if (!font) {
        font = [LGL objectPool][name]
        =
        [[LGLFont alloc] initWithFontName:name];
    }

    return font;
}

- (NSArray *)renderersForAttributedString:(NSAttributedString *)string
{
    NSMutableArray      *renderers  = [NSMutableArray new];

    CTFramesetterRef    framesetter = CTFramesetterCreateWithAttributedString(Voidify(string));
    CTFrameRef          textframe   = CTFramesetterCreateFrame(framesetter, ((CFRange){0}), _textPath, NULL);

    CGFloat             vertical    = 0.;

    for (id object in Objectify(CTFrameGetLines(textframe))) {
        CTLineRef   line    = Voidify(object);

        CGFloat     ascender, descender, leading;
        CTLineGetTypographicBounds(line, &ascender, &descender, &leading);

        for (id object in Objectify(CTLineGetGlyphRuns(line))) {
            CTRunRef    run         = Voidify(object);
            CFRange     range       = CTRunGetStringRange(run);

            CTFontRef   runFont     = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
            CGFloat     runSize     = CTFontGetSize(runFont);
            
            UIColor     *color      = [string colorAtIndex:range.location];
            LGLFont     *font       = [self fontForTextRun:run];
            
            for (NSUInteger
                 index = 0,
                 count = CTRunGetGlyphCount(run);
                 index < count;
                 index ++) {
                CFRange range = ((CFRange){index, 1});

                CGGlyph glyph ; CTRunGetGlyphs(run, range, &glyph);

                LGLQuad *quad = [font rendererForGlyph:glyph pointSize:runSize];

                if (quad) {
                    CGPoint point ; CTRunGetPositions(run, range, &point);
                    CGSize  trans ; CTFontGetVerticalTranslationsForGlyphs(runFont, &glyph, &trans, 1);
                    CGRect  frame ; CTFontGetBoundingRectsForGlyphs(runFont, kCTFontDefaultOrientation, &glyph, &frame, 1);

                    CGRect  rect  = [quad target];
                    rect.origin.x = (_frame.origin.x + kLGLTextHOffset);
                    rect.origin.y = (_frame.origin.y + kLGLTextVOffset);
                    rect.origin.x += point.x + frame.origin.x;
                    rect.origin.y += trans.height + vertical + ascender;

                    quad.target = rect;
                    quad.color  = color;

                    [renderers addObject:quad];
                }
            }
        }

        vertical += (ascender + descender + leading);
    }

    CFRelease(textframe);
    CFRelease(framesetter);

    return renderers;
}

@end
