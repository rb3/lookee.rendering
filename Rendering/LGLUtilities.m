//
//  LGLUtilities.c
//  Writeability
//
//  Created by Ryan on 12/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLUtilities.h"



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLUtilities
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Externals
//*********************************************************************************************************************//

const LGLMatrix4x4 LGLMatrix4x4Identity = ((LGLMatrix4x4) {
    1., 0., 0., 0.,
    0., 1., 0., 0.,
    0., 0., 1., 0.,
    0., 0., 0., 1.
});


LGLMatrix4x2 LGLTexVertices(LGLMatrix2x2 target, NSUInteger surfaceWidth, NSUInteger surfaceHeight) {
    LGLMatrix4x2 vertices = LGLRectVertices(target.rect);
    
    LGLMatrix4x2 texVertices;
    texVertices.f00 = +LFMLinearInterpolate(-1., +1., vertices.f00 / surfaceWidth);
    texVertices.f01 = -LFMLinearInterpolate(-1., +1., vertices.f01 / surfaceHeight);

    texVertices.f10 = +LFMLinearInterpolate(-1., +1., vertices.f10 / surfaceWidth);
    texVertices.f11 = -LFMLinearInterpolate(-1., +1., vertices.f11 / surfaceHeight);

    texVertices.f20 = +LFMLinearInterpolate(-1., +1., vertices.f20 / surfaceWidth);
    texVertices.f21 = -LFMLinearInterpolate(-1., +1., vertices.f21 / surfaceHeight);

    texVertices.f30 = +LFMLinearInterpolate(-1., +1., vertices.f30 / surfaceWidth);
    texVertices.f31 = -LFMLinearInterpolate(-1., +1., vertices.f31 / surfaceHeight);

    return texVertices;
}

LGLMatrix4x2 LGLTexCoords(LGLMatrix2x2 region, NSUInteger textureWidth, NSUInteger textureHeight) {
    LGLMatrix4x2 vertices = LGLRectVertices(region.rect);

    LGLMatrix4x2 texCoords;
    texCoords.f00 = LFMLinearInterpolate(0., 1., vertices.f10 / textureWidth);
    texCoords.f01 = LFMLinearInterpolate(0., 1., vertices.f11 / textureHeight);

    texCoords.f10 = LFMLinearInterpolate(0., 1., vertices.f00 / textureWidth);
    texCoords.f11 = LFMLinearInterpolate(0., 1., vertices.f01 / textureHeight);

    texCoords.f20 = LFMLinearInterpolate(0., 1., vertices.f30 / textureWidth);
    texCoords.f21 = LFMLinearInterpolate(0., 1., vertices.f31 / textureHeight);

    texCoords.f30 = LFMLinearInterpolate(0., 1., vertices.f20 / textureWidth);
    texCoords.f31 = LFMLinearInterpolate(0., 1., vertices.f21 / textureHeight);
    
    return texCoords;
}

extern LGLMatrix4x2 LGLTexVerticesInverted(LGLMatrix2x2 target, NSUInteger surfaceWidth, NSUInteger surfaceHeight) {
    LGLMatrix4x2 vertices = LGLRectVertices(target.rect);

    LGLMatrix4x2 texVertices;
    texVertices.f00 = +LFMLinearInterpolate(-1., +1., vertices.f10 / surfaceWidth);
    texVertices.f01 = -LFMLinearInterpolate(-1., +1., vertices.f11 / surfaceHeight);

    texVertices.f10 = +LFMLinearInterpolate(-1., +1., vertices.f00 / surfaceWidth);
    texVertices.f11 = -LFMLinearInterpolate(-1., +1., vertices.f01 / surfaceHeight);

    texVertices.f20 = +LFMLinearInterpolate(-1., +1., vertices.f30 / surfaceWidth);
    texVertices.f21 = -LFMLinearInterpolate(-1., +1., vertices.f31 / surfaceHeight);

    texVertices.f30 = +LFMLinearInterpolate(-1., +1., vertices.f20 / surfaceWidth);
    texVertices.f31 = -LFMLinearInterpolate(-1., +1., vertices.f21 / surfaceHeight);

    return texVertices;
}

extern LGLMatrix4x2 LGLTexCoordsInverted(LGLMatrix2x2 region, NSUInteger textureWidth, NSUInteger textureHeight) {
    LGLMatrix4x2 vertices = LGLRectVertices(region.rect);

    LGLMatrix4x2 texCoords;
    texCoords.f00 = LFMLinearInterpolate(0., 1., vertices.f00 / textureWidth);
    texCoords.f01 = LFMLinearInterpolate(0., 1., vertices.f01 / textureHeight);

    texCoords.f10 = LFMLinearInterpolate(0., 1., vertices.f10 / textureWidth);
    texCoords.f11 = LFMLinearInterpolate(0., 1., vertices.f11 / textureHeight);

    texCoords.f20 = LFMLinearInterpolate(0., 1., vertices.f20 / textureWidth);
    texCoords.f21 = LFMLinearInterpolate(0., 1., vertices.f21 / textureHeight);

    texCoords.f30 = LFMLinearInterpolate(0., 1., vertices.f30 / textureWidth);
    texCoords.f31 = LFMLinearInterpolate(0., 1., vertices.f31 / textureHeight);

    return texCoords;
}

LGLVector2 LGLTargetLocation(LGLMatrix4x2 texVertices, NSUInteger targetWidth, NSUInteger targetHeight) {
    GLfloat tlpXFactor  = +LFMReverseLinearInterpolate(-1., +1., texVertices.f00);
    GLfloat trpXFactor  = +LFMReverseLinearInterpolate(-1., +1., texVertices.f20);

    GLfloat tlpYFactor  = -LFMReverseLinearInterpolate(-1., +1., texVertices.f01);
    GLfloat brpYFactor  = -LFMReverseLinearInterpolate(-1., +1., texVertices.f31);

    GLfloat xRatio      = LFMDivf(tlpXFactor, trpXFactor);
    GLfloat yRatio      = LFMDivf(tlpYFactor, brpYFactor);

    LGLVector2 location;
    location.x = LFMDivf(xRatio * targetWidth, 1.- xRatio);
    location.y = LFMDivf(yRatio * targetHeight, 1.- yRatio);

    return location;
}

LGLVector2 LGLSurfaceDimensions(LGLMatrix4x2 texVertices, NSUInteger targetWidth, NSUInteger targetHeight) {
    LGLVector2 location     = LGLTargetLocation(texVertices, targetWidth, targetHeight);

    GLfloat     tlpX        = LFMReverseLinearInterpolate(-1., +1., texVertices.f00);
    GLfloat     tlpY        = LFMReverseLinearInterpolate(-1., +1., texVertices.f01);

    LGLVector2 dimensions;
    dimensions.w = +LFMDivf(location.x, tlpX);
    dimensions.h = -LFMDivf(location.y, tlpY);

    return dimensions;
}

LGLMatrix4x2 LGLRectVertices(CGRect rect) {
    CGPoint bottomLeft  = CGRectGetBottomLeftPoint(rect);
    CGPoint topLeft     = CGRectGetTopLeftPoint(rect);
    CGPoint bottomRight = CGRectGetBottomRightPoint(rect);
    CGPoint topRight    = CGRectGetTopRightPoint(rect);

    LGLMatrix4x2 vertices;
    vertices.f00 = bottomLeft.x;
    vertices.f01 = bottomLeft.y;

    vertices.f10 = topLeft.x;
    vertices.f11 = topLeft.y;

    vertices.f20 = bottomRight.x;
    vertices.f21 = bottomRight.y;

    vertices.f30 = topRight.x;
    vertices.f31 = topRight.y;

    return vertices;
}

LGLMatrix4x4 LGLInterleaveVertices(LGLMatrix4x2 vertices, LGLMatrix4x2 other) {
    LGLMatrix4x4 interleaved;

    GLuint length = (sizeof(interleaved) / sizeof(*interleaved.v2));

    for (GLuint index = 0; index < length; ++ index) {
        if (IsEven(index)) {
            interleaved.v2[index] = vertices.v2[(index-0) /2];
        } else {
            interleaved.v2[index] = other.v2[(index-1) /2];
        }
    }

    return interleaved;
}