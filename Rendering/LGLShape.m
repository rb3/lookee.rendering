//
//  LGLShape.m
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LGLShape.h"

#import "LGLProgram.h"
#import "LGLCanvas.h"

#import <Utilities/UIColor+Utilities.h>
#import <Utilities/NSData+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLShape
#pragma mark -
//*********************************************************************************************************************//




@interface LGLShape ()

@property (nonatomic, readwrite , strong) NSData *transformData;
@property (nonatomic, readwrite , strong) NSData *colorData;

@end

@implementation LGLShape

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFormat:(NSString *)format __ILLEGALMETHOD__;

- (instancetype)initWithTransform:(LGLMatrix4x4)transform
{
	if ((self = [super initWithFormat:@"aPosition:p2f"])) {
        [self setTransform:transform];
        [self setColor:nil];
	}

	return self;
}


#pragma mark - LGLRenderable
//*********************************************************************************************************************//

- (void)render
{
    if ([self length]) {
        LGLProgram *program
        =
        ([self program]?:
         [LGL objectPool]
         [kLGLProgramDefaultVBOKey]);

        [program use];

        [program setData:_transformData forUniformWithID:@"uTransform"];
        [program setData:_colorData forUniformWithID:@"uColor"];

        [self blit];
    }
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setTransform:(LGLMatrix4x4)transform
{
    {_transform = transform;}

    _transformData = Datafy(transform);
}

- (void)setColor:(UIColor *)color
{
    {_color = color;}

    LGLVector4 components;

    [_color?:[UIColor blackColor]
     getComponents:components.v];

    _colorData = Datafy(components);
}

@end