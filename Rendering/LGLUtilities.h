//
//  LGLUtilities.h
//  Writeability
//
//  Created by Ryan on 12/6/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//



#import <Utilities/LFastMath.h>
#import <Utilities/LGeometry.h>



#if defined(__ARM_NEON__)
#   include <arm_neon.h>
#endif



#ifdef __cplusplus
extern "C" {
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLUtilities
#pragma mark -
//*********************************************************************************************************************//

    


#pragma mark - Types
//*********************************************************************************************************************//

typedef wchar_t LGLChar;

typedef union LGLVector2
{
    struct { float x, y; };
    struct { float s, t; };
    struct { float w, h; };

    float v[2];

    /* Geometry Conversion Members */
    CGPoint point;
} __attribute__((aligned(8))) LGLVector2;

typedef union LGLIVector2
{
    struct { int x, y; };
    struct { int s, t; };
    struct { int w, h; };

    int v[2];
} __attribute__((aligned(8))) LGLIVector2;

typedef union LGLSVector2
{
    struct { short x, y; };
    struct { short s, t; };
    struct { short w, h; };

    short v[2];
} __attribute__((aligned(4))) LGLSVector2;

typedef union LGLVector4
{
    struct { float x, y, z, w; };
    struct { float r, g, b, a; };
    struct { float s, t, p, q; };

    struct {
        union { LGLVector2 xy, rg, st; };
        union { LGLVector2 zw, ba, pq; };
    };

    LGLVector2 v2[2];

    float v[4];
} __attribute__((aligned(16))) LGLVector4;

typedef union LGLIVector4
{
    struct { int x, y, z, w; };
    struct { int r, g, b, a; };
    struct { int s, t, p, q; };

    struct {
        union { LGLIVector2 xy, rg, st; };
        union { LGLIVector2 zw, ba, pq; };
    };

    LGLIVector2 v2[2];

    int v[4];
} __attribute__((aligned(16))) LGLIVector4;

typedef union LGLMatrix2x2
{
    struct {
        float
        f00, f01,
        f10, f11;
    };

    struct {
        float
        x, y,
        w, h;
    };

    LGLVector2 v2[2];

    float m[4];

    /* Geometry Conversion Members */
    CGRect rect;
} LGLMatrix2x2;

typedef union LGLIMatrix2x2
{
    struct {
        int
        i00, i01,
        i10, i11;
    };

    struct {
        int
        x, y,
        w, h;
    };

    LGLIVector2 v2[2];

    int m[4];
} LGLIMatrix2x2;

typedef union LGLMatrix4x2
{
    struct {
        float
        f00, f01,
        f10, f11,
        f20, f21,
        f30, f31;
    };

    LGLMatrix2x2 m2x2[2];

    LGLVector2 v2[4];
    LGLVector4 v4[2];

    float m[8];
} LGLMatrix4x2;

typedef union LGLMatrix4x4
{
    struct {
        float
        f00, f01, f02, f03,
        f10, f11, f12, f13,
        f20, f21, f22, f23,
        f30, f31, f32, f33;
    };

    LGLMatrix2x2 m2x2[4];
    LGLMatrix4x2 m4x2[2];

    LGLVector2 v2[8];
    LGLVector4 v4[4];

    float m[16];
} __attribute__((aligned(16))) LGLMatrix4x4;



#pragma mark - Generic Functions
//*********************************************************************************************************************//

extern LGLMatrix4x2 LGLTexVertices(LGLMatrix2x2 target, NSUInteger surfaceWidth, NSUInteger surfaceHeight);
extern LGLMatrix4x2 LGLTexCoords(LGLMatrix2x2 region, NSUInteger textureWidth, NSUInteger textureHeight);

extern LGLMatrix4x2 LGLTexVerticesInverted(LGLMatrix2x2 target, NSUInteger surfaceWidth, NSUInteger surfaceHeight);
extern LGLMatrix4x2 LGLTexCoordsInverted(LGLMatrix2x2 region, NSUInteger textureWidth, NSUInteger textureHeight);

extern LGLVector2 LGLTargetLocation(LGLMatrix4x2 texVertices, NSUInteger targetWidth, NSUInteger targetHeight);
extern LGLVector2 LGLSurfaceDimensions(LGLMatrix4x2 texVertices, NSUInteger targetWidth, NSUInteger targetHeight);

extern LGLMatrix4x2 LGLRectVertices(CGRect rect);

extern LGLMatrix4x4 LGLInterleaveVertices(LGLMatrix4x2 vertices, LGLMatrix4x2 other);


#pragma mark - LGLVector2
//*********************************************************************************************************************//

static inline LGLVector2 LGLVector2Subtract(LGLVector2 vectorLeft, LGLVector2 vectorRight) {
#ifdef __ARM_NEON__
    float32x2_t v = vsub_f32(*(float32x2_t *)&vectorLeft, *(float32x2_t *)&vectorRight);

    return *((LGLVector2 *)&v);
#else
    return ((LGLVector2) {
        vectorLeft.v[0] - vectorRight.v[0],
        vectorLeft.v[1] - vectorRight.v[1]
    });
#endif
}

static inline float LGLVector2Length(LGLVector2 vector) {
#ifdef __ARM_NEON__
        float32x2_t v = vmul_f32(*(float32x2_t *)&vector, *(float32x2_t *)&vector);

        v = vpadd_f32(v, v);

        return LFMSqrtf(vget_lane_f32(v, 0));
#else
        return LFMSqrtf(vector.v[0] * vector.v[0] + vector.v[1] * vector.v[1]);
#endif
}

static inline float LGLVector2Distance(LGLVector2 vectorStart, LGLVector2 vectorEnd) {
    return LGLVector2Length(LGLVector2Subtract(vectorEnd, vectorStart));
}

static inline LGLVector2 LGLVector2Lerp(LGLVector2 vectorStart, LGLVector2 vectorEnd, float t) {
#ifdef __ARM_NEON__
    float32x2_t vDiff = vsub_f32(*(float32x2_t *)&vectorEnd, *(float32x2_t *)&vectorStart);

    vDiff = vmul_f32(vDiff, vdup_n_f32((float32_t)t));
    float32x2_t v = vadd_f32(*(float32x2_t *)&vectorStart, vDiff);

    return *((LGLVector2 *)&v);
#else
    return ((LGLVector2) {
        vectorStart.v[0] + ((vectorEnd.v[0] - vectorStart.v[0]) * t),
        vectorStart.v[1] + ((vectorEnd.v[1] - vectorStart.v[1]) * t)
    });
#endif
}


#pragma mark - LGLIMatrix2x2
//*********************************************************************************************************************//

static inline BOOL LGLIMatrix2x2AllEqualToIMatrix2x2(LGLIMatrix2x2 matrixLeft, LGLIMatrix2x2 matrixRight) {
    return (matrixLeft.m[0] == matrixRight.m[0] &&
            matrixLeft.m[1] == matrixRight.m[1] &&
            matrixLeft.m[2] == matrixRight.m[2] &&
            matrixLeft.m[3] == matrixRight.m[3]);
}


#pragma mark - LGLMatrix4x4
//*********************************************************************************************************************//

extern const LGLMatrix4x4 LGLMatrix4x4Identity;

static inline LGLMatrix4x4 LGLMatrix4x4MakeOrtho(float left, float right, float bottom, float top, float nearZ, float farZ) {
    float ral = right + left;
    float rsl = right - left;
    float tab = top + bottom;
    float tsb = top - bottom;
    float fan = farZ + nearZ;
    float fsn = farZ - nearZ;

    return ((LGLMatrix4x4) {
        (float)2./ rsl  , 0.            , 0.                , 0.,
        0.              , (float)2./ tsb, 0.                , 0.,
        0.              , 0.            , (float)-2./ fsn   , 0.,
        -ral / rsl      , -tab / tsb    , -fan / fsn        , 1.
    });
}


#ifdef __cplusplus
}
#endif

