//
//  LGLProgram.h
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLObject.h"



extern NSString * const kLGLProgramDefaultTexKey;
extern NSString * const kLGLProgramDefaultVBOKey;
extern NSString * const kLGLProgramColoredTexKey;
extern NSString * const kLGLProgramSDFieldTexKey;



@interface LGLProgram : LGLObject

@property (nonatomic, readwrite , strong) NSArray                   *shaders;
@property (nonatomic, readwrite , strong) NSDictionary              *attributes;


+ (LGLProgram *)current;


- (BOOL)load;
- (BOOL)use;
- (void)reset;

- (void)setData:(NSData *)data forAttributeWithID:(id <NSCopying>)ID;
- (void)setData:(NSData *)data forUniformWithID:(id <NSCopying>)ID;

- (void)setProperties:(NSDictionary *)properties forAttributeWithID:(id <NSCopying>)ID;

@end
