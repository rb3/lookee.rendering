//
//  LGLShaderAPositionUColorTexture.h
//  Writeability
//
//  Created by Ryan on 1/17/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


@$_quote
(
 varying mediump vec2 vTexCoords;

 @if defined VERTEX_SHADER;

 uniform    mat4 uTransform;
 attribute  vec4 aPosition;
 attribute  vec2 aTexCoords;

 void main(void) {
     gl_Position    = uTransform * aPosition;
     vTexCoords     = aTexCoords;
 }

 @elif defined FRAGMENT_SHADER;

 precision lowp float;

 uniform sampler2D  uSampler;
 uniform vec4       uColor;

 void main(void) {
     gl_FragColor = uColor * texture2D(uSampler, vTexCoords).a;
 }

 @endif;
 );