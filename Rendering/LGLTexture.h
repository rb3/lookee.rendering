//
//  LGLImage.h
//  Writeability
//
//  Created by Ryan on 10/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLObject.h"



@interface LGLTexture : LGLObject

@property (nonatomic, readonly  , assign) NSUInteger    width;
@property (nonatomic, readonly  , assign) NSUInteger    height;
@property (nonatomic, readonly  , assign) NSUInteger    depth;
@property (nonatomic, readonly  , assign) float         scale;

@property (nonatomic, readonly  , strong) NSData        *data;

@property (nonatomic, readonly  , assign) NSUInteger    rows;
@property (nonatomic, readonly  , assign) NSUInteger    columns;
@property (nonatomic, readonly  , assign) NSUInteger    bytesPerRow;



- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
data            :(NSData *  )data;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
data            :(NSData *  )data;


- (CGContextRef)mappedContext;

- (UIImage *)imageWithScale:(float)scale;

- (UIColor *)colorAtPoint:(CGPoint)point;
- (void)setColor:(UIColor *)color atPoint:(CGPoint)point;


- (void)
setBitmap   :(NSData *  )bitmap
withRows    :(NSUInteger)rows
stride      :(NSUInteger)stride
atRow       :(NSUInteger)row
column      :(NSUInteger)column;

- (void)drawBitmap:(void(^)(void))block;

- (void)clear;

@end