//
//  LGLViewport.m
//  Writeability
//
//  Created by Ryan on 8/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//



#import "LGLSurface.h"

#import "LGLCanvas.h"
#import "LGLQuad.h"

#import <OpenGLES/ES2/glext.h>


/**
 * TODO:
 * 1. Use Apple fence OpenGL objects to sync between threads
 */




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LGLLayer : CAEAGLLayer

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLViewport
#pragma mark -
// Primarily based on the Cocos2D implementation of CCGLView @ http://www.cocos2d-iphone.org/
// Partially based on Nick Lockwood's GLView @ https://github.com/nicklockwood/GLView
//*********************************************************************************************************************//




@interface LGLSurface ()

@property (nonatomic, readonly, assign) NSUInteger  rows;
@property (nonatomic, readonly, assign) NSUInteger  columns;

@property (nonatomic, readonly, assign) BOOL        batchOperation;
@property (nonatomic, readonly, assign) BOOL        needsRender;

@property (nonatomic, readonly, strong) LGLQuad     *renderer;
@property (nonatomic, readonly, strong) LGLCanvas   *canvas;

@property (nonatomic, readonly, assign) GLuint      mainFrameBuffer;
@property (nonatomic, readonly, assign) GLuint      mainRenderBuffer;
@property (nonatomic, readonly, assign) GLuint      msaaFrameBuffer;
@property (nonatomic, readonly, assign) GLuint      msaaRenderBuffer;

@end


@implementation LGLSurface

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
	BOOL    __initialized;
    LGLQuad *__renderer;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;
- (instancetype)initWithFrame:(CGRect)frame __ILLEGALMETHOD__;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
samples         :(NSUInteger)samples
{
	DBGParameterAssert((width  > 0) &&
                        (height > 0) &&
                        !IsZero(scale));

	if ((self = [super initWithFrame:((CGRect){{0}, {width, height}})])) {
        _width      = width;
        _height     = height;
        _scale      = scale;
        
        _rows       = (height * scale);
        _columns    = (width  * scale);

		GLint maxSamples;
        glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamples);

		_samples    = MIN(maxSamples, samples);

        [self setContentMode:UIViewContentModeTopLeft];
        [self setBackgroundColor:[UIColor whiteColor]];

        [(id)[self layer] setShouldRasterize:YES];

		[(id)[self layer] setDrawableProperties:
		 (@{
		  kEAGLDrawablePropertyRetainedBacking	: @NO,
		  kEAGLDrawablePropertyColorFormat		: kEAGLColorFormatRGBA8
		  })];
	}

	return self;
}

- (void)dealloc
{
    GLuint mainFrameBuffer  = _mainFrameBuffer;
    GLuint mainRenderBuffer = _mainRenderBuffer;

    GLuint msaaFrameBuffer  = _msaaFrameBuffer;
    GLuint msaaRenderBuffer = _msaaRenderBuffer;

	[LGL queueAsyncOperation:^{
		[LGL deleteFrameBuffer:mainFrameBuffer];
        [LGL deleteRenderBuffer:mainRenderBuffer];
        [LGL deleteFrameBuffer:msaaFrameBuffer];
        [LGL deleteRenderBuffer:msaaRenderBuffer];

        glFlush();
	}];

	DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

+ (Class)layerClass
{
	return [LGLLayer class];
}

- (void)setFrame:(CGRect)frame
{
	DBGParameterAssert
    (CGRectIsEmpty(self.frame) ||
     CGSizeEqualToSize(self.frame.size, frame.size));

	[super setFrame:frame];
}

- (void)layoutSubviews
{
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
}

- (void)setNeedsDisplay
{
    if (_batchOperation == YES) {
        {_needsRender = YES;}
    } else {
        {_needsRender = NO;}

        [super setNeedsDisplay];
    }
}

- (void)setNeedsDisplayInRect:(CGRect)rect
{
    [self setNeedsDisplay];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (LGLQuad *)renderer
{
	NSString    *key        = (@"surface-renderer");
	LGLQuad     *renderer   = [LGL objectPool][key];

	if (!renderer) {
        LGLCanvas *canvas   = [[LGLCanvas alloc]
                               initWithWidth  :kLGLSurfaceCanvasDimension
                               height         :kLGLSurfaceCanvasDimension
                               scale          :[self scale]
                               samples        :[self samples]];

        [canvas setBackgroundColor:self.backgroundColor];

        renderer = [LGL objectPool][key]
        =
        [[LGLQuad alloc] initWithTexture:canvas];
	}

    return renderer;
}

- (LGLCanvas *)canvas
{
    return (id)(self.renderer.texture);
}

- (void)connectToSurface:(LGLSurface *)connectedSurface
{
	if (_connectedSurface != connectedSurface) {
		{_connectedSurface = connectedSurface;}

		[LGL queueSyncOperation:^{
			[_connectedSurface connectToSurface:self];

            DBGAssert
            ((_rows     ==	_connectedSurface.rows      )  &&
             (_columns  ==	_connectedSurface.columns   )  &&
             (_samples  ==	_connectedSurface.samples   ));
		}];
	}
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)batchRenderOperations:(void(^)(void))block
{
    [LGL queueAsyncOperation:^{
        BOOL nested = _batchOperation;

        if (!nested) {
            {_batchOperation = YES;}
        }

        if (block) {
            block();
        }

        if (!nested) {
            {_batchOperation = NO;}

            if (_needsRender) {
                [self setNeedsDisplay];
            }
        }
    }];
}

- (void)queueRenderOperation:(void(^)(void))block
{
    [self.canvas queueRenderOperation:block];
}

- (void)clear
{
    [self.canvas clear];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)present
{
	[LGL queueSyncOperation:^{
		if ([self initialize])  {
			if (_samples > 0) {
                [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_FRAMEBUFFER];
			} else {
                [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
			}

            [LGL setViewWidth:_columns height:_rows];
            [LGL setScale:_scale];
            [LGL setClearColor:self.backgroundColor];

            glClear(GL_COLOR_BUFFER_BIT);

			[self.renderer render];

			if (_samples > 0) {
                [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_READ_FRAMEBUFFER_APPLE];
                [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_DRAW_FRAMEBUFFER_APPLE];
				glResolveMultisampleFramebufferAPPLE();

				glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, ((GLenum[]){GL_COLOR_ATTACHMENT0}));
			}

			[LGL presentRenderBuffer:_mainRenderBuffer];
		}
	}];
}

- (GLboolean)initialize
{
	if (__initialized) {
        return GL_TRUE;
    }

    glGenFramebuffers(1, &_mainFrameBuffer);

    if (_mainFrameBuffer) {
        [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
        glGenRenderbuffers(1, &_mainRenderBuffer);

        if (_mainRenderBuffer) {
            [LGL bindRenderBuffer:_mainRenderBuffer toTarget:GL_RENDERBUFFER];
            glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                                      GL_COLOR_ATTACHMENT0,
                                      GL_RENDERBUFFER,
                                      _mainRenderBuffer);

            if (_samples > 0) {
                glGenFramebuffers(1, &_msaaFrameBuffer);

                if (_msaaFrameBuffer) {
                    [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_FRAMEBUFFER];
                } else {
                    DBGWarning(@"Failed to create anti-aliasing framebuffer object (0x%X)", glGetError());

                    return GL_FALSE;
                }
            }

            if ([LGL setDrawable:(id)[self layer]]) {
#if DEBUG
                GLint rows;
                GLint columns;

                glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &columns);
                glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &rows);

                DBGAssert((columns == _columns) && (rows == _rows));
#endif
                if (_samples > 0) {
                    glGenRenderbuffers(1, &_msaaRenderBuffer);
                    
                    if (_msaaRenderBuffer) {
                        [LGL bindRenderBuffer:_msaaRenderBuffer toTarget:GL_RENDERBUFFER];
                        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER,
                                                              _samples,
                                                              GL_RGBA8_OES,
                                                              _columns,
                                                              _rows);
                        glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                                                  GL_COLOR_ATTACHMENT0,
                                                  GL_RENDERBUFFER,
                                                  _msaaRenderBuffer);

                        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

                        if (status != GL_FRAMEBUFFER_COMPLETE) {
                            DBGWarning(@"Failed to make complete anti-aliasing framebuffer object (0x%X)", status);
                            
                            return GL_FALSE;
                        }
                    } else {
                        DBGWarning(@"Failed to create anti-aliasing renderbuffer object (0x%X)", glGetError());
                        
                        return GL_FALSE;
                    }
                    
                    [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
                }
                
                GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
                
                if (status == GL_FRAMEBUFFER_COMPLETE) {
                    __initialized = YES;
                } else {
                    DBGWarning(@"Failed to make complete framebuffer object (0x%X)", status);
                    
                    return GL_FALSE;
                }
            } else {
                DBGWarning(@"Failed to call context (0x%X)", glGetError());
                
                return GL_FALSE;
            }
        } else {
            DBGWarning(@"Failed to create color renderbuffer object (0x%X)", glGetError());
            
            return GL_FALSE;
        }
    } else {
        DBGWarning(@"Failed to create main framebuffer object (0x%X)", glGetError());
        
        return GL_FALSE;
    }

	return GL_TRUE;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLLayer
#pragma mark -
// Partially based on Nick Lockwood's GLView @ https://github.com/nicklockwood/GLView
//*********************************************************************************************************************//




@implementation LGLLayer

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (CGFloat)contentsScale
{
    return [(LGLSurface *)[self delegate] scale];
}

- (CGFloat)rasterizationScale
{
    return [(LGLSurface *)[self delegate] scale];
}

- (void)display
{
    LGLSurface *viewport = [self delegate];

    if ([viewport delegate]) {
        [[viewport delegate]
         surface        :viewport
         renderInRect   :viewport.bounds];
    } else {
        [viewport drawRect:viewport.bounds];
    }

    [viewport present];

    if ([viewport connectedSurface]) {
        [viewport.connectedSurface present];
    }
}

- (void)renderInContext:(CGContextRef)ctx
{
	LGLSurface  *view       = [self delegate];
	UIImage     *snapshot   = [view.canvas imageWithScale:view.scale];

	UIGraphicsPushContext(ctx);
	[snapshot drawAtPoint:CGPointZero];
	UIGraphicsPopContext();

	for (CALayer *layer in [self sublayers]) {
        CGContextSaveGState(ctx);
        CGContextTranslateCTM(ctx, layer.frame.origin.x, layer.frame.origin.y);
		
        [layer renderInContext:ctx];

        CGContextRestoreGState(ctx);
    }
}

@end