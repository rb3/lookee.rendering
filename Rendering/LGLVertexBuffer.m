//
//  LGLVertexBuffer.m
//  Writeability
//
//  Created by Ryan on 1/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLVertexBuffer.h"

#import "LGLProgram.h"

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSString+Utilities.h>

#import <OpenGLES/ES2/glext.h>

/**
 * TODO: Limit VBO size to 65536 * 2 * sizeof(GLfloat)
 * TODO: Flatten oldest strokes once frame rate starts bogging down
 * TODO: Eliminate collinear points in a line for faster rendering
 * TODO: Run the all rendering on a global rendering thread
 */




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LGLVertexAttribute : NSObject
{
@public
    NSString   *identifier;
    GLsizeiptr size;

    GLuint     length;
    GLenum     type;
    GLboolean  normalized;
    GLsizeiptr index;

}
@end


@interface LGLVertexAttributeParser : LGLObject

+ (NSDictionary *)parse:(NSString *)string;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLVertexBuffer
#pragma mark -
//*********************************************************************************************************************//




@interface LGLVertexBuffer ()

@property (nonatomic, readonly  , strong) NSArray           *attributes;

@property (nonatomic, readonly  , assign) GLsizei           stride;

@property (nonatomic, readonly  , assign) GLuint            handle;
@property (nonatomic, readonly  , assign) GLuint            buffer;

@property (nonatomic, readwrite , assign) GLsizeiptr        size;
@property (nonatomic, readwrite , assign) GLsizeiptr        capacity;

@property (nonatomic, readonly  , strong) NSMutableData     *queuedData;

@end

@implementation LGLVertexBuffer

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    BOOL            __initialized;

    NSMutableData   *_vertexData;

    NS_OPTIONS(Byte, _actionType)
    {
        kLGLVBActionTypeNone    = 0 << 0,
        kLGLVBActionTypeAssign  = 1 << 0,
        kLGLVBActionTypeAppend  = 1 << 1,
        kLGLVBActionTypePack    = 1 << 2
    };
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithFormat:(NSString *)format
{
    if ((self = [super init])) {
        _format     = [format copy];
        _vertexData = [NSMutableData new];
        _queuedData = [NSMutableData new];

        [self setValuesForKeysWithDictionary:[LGLVertexAttributeParser parse:format]];
    }

    return self;
}

- (void)dealloc
{
    GLuint handle = _handle;
    GLuint buffer = _buffer;

	[LGL queueAsyncOperation:^{
        [LGL deleteVertexArray:handle];
        [LGL deleteVertexBuffer:buffer];
	}];

	DBGMessage(@"deallocated %@", self);
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setVertexData:(NSData *)data
{
    {_actionType |= kLGLVBActionTypeAssign;}

    if ([data length]) {
        {_length = 1;}
    } else {
        {_actionType |= kLGLVBActionTypePack;}

        {_length = 0;}
    }

    [_queuedData setData:data];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)appendVertexData:(NSData *)data
{
    if ([data length]) {
        {_actionType |= kLGLVBActionTypeAppend;}

        {++ _length;}

        [_queuedData appendData:data];
    }
}

- (void)packVertices
{
    if (_length) {
        {_actionType |= kLGLVBActionTypePack;}
    }
}

- (void)blit
{
    if (_length) {
        [self executeActions];

        [LGL bindVertexArray:_handle];
        glDrawArrays(GL_TRIANGLE_STRIP, 0, (_size / _stride));
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)executeActions
{
    BOOL assign = (_actionType & kLGLVBActionTypeAssign);
    BOOL append = (_actionType & kLGLVBActionTypeAppend);
    BOOL pack   = (_actionType & kLGLVBActionTypePack);

    if (assign || append) {
        if (assign) {
            [_vertexData setData:_queuedData];
        } else {
            [_vertexData appendData:_queuedData];
        }

        GLsizeiptr increment    = [_queuedData length];
        GLsizeiptr capacity     = [_vertexData length];

        if (pack) {
            [self allocateBufferWithCapacity:capacity usageHint:GL_STATIC_DRAW];
            [self insertBufferData:_vertexData.bytes withSize:capacity];
        } else {
            if (_capacity < capacity) {
                if (assign) {
                    [self allocateBufferWithCapacity:capacity*1 usageHint:GL_DYNAMIC_DRAW];
                } else {
                    [self allocateBufferWithCapacity:capacity*2 usageHint:GL_DYNAMIC_DRAW];
                }

                [self insertBufferData:_vertexData.bytes withSize:capacity];
            } else {
                if (assign) {
                    [self insertBufferData:_queuedData.bytes withSize:increment];
                } else {
                    [self pushBufferData:_queuedData.bytes withSize:increment];
                }
            }
        }
    } else if (pack) {
        [self allocateBufferWithCapacity:_size usageHint:GL_STATIC_DRAW];
        [self insertBufferData:_vertexData.bytes withSize:_size];
    }

    {_actionType = kLGLVBActionTypeNone;}

    [_queuedData setData:nil];
}

- (GLboolean)initialize
{
	if (!__initialized) {
        glGenVertexArraysOES(1, &_handle);

        if (_handle) {
            glGenBuffers(1, &_buffer);

            if (_buffer) {
                [LGL bindVertexArray:_handle];
                [LGL bindVertexBuffer:_buffer toTarget:GL_ARRAY_BUFFER];

                for (LGLVertexAttribute *attribute in _attributes) {
                    LGLProgram *program = [LGLProgram current];

                    DBGAssert(program);
                    {
                        [program
                         setProperties      :
                         (@{ @"stride"      : @(self.stride),
                             @"length"      : @(attribute->length),
                             @"type"        : @(attribute->type),
                             @"normalized"  : @(attribute->normalized),
                             @"index"       : @(attribute->index) })
                         forAttributeWithID :attribute->identifier];

                        {__initialized = YES;}
                    }
                }
            } else {
                DBGWarning(@"Failed to create vertex buffer object (0x%X)", glGetError());

                return GL_FALSE;
            }
        } else {
            DBGWarning(@"Failed to create vertex array object (0x%X)", glGetError());
            
            return GL_FALSE;
        }
    }
    
	return GL_TRUE;
}

- (GLvoid) allocateBufferWithCapacity:(GLsizeiptr)capacity usageHint:(GLenum)hint
{
	if (_capacity != capacity) {
        if ([self initialize]) {
            [LGL unbindVertexArray];
            [LGL bindVertexBuffer:_buffer toTarget:GL_ARRAY_BUFFER];
            glBufferData(GL_ARRAY_BUFFER, capacity, NULL, hint);

            {_capacity = capacity;}
        }
    }
}

- (GLvoid)insertBufferData:(const GLubyte *)data withSize:(GLsizeiptr)size
{
	{_size = 0;}

	[self pushBufferData:data withSize:size];
}

- (GLvoid)pushBufferData:(const GLubyte *)data withSize:(GLsizeiptr)size
{
	if (size && data) {
        [LGL unbindVertexArray];
		[LGL bindVertexBuffer:_buffer toTarget:GL_ARRAY_BUFFER];

        GLfloat	*mapped	=
        glMapBufferRangeEXT(GL_ARRAY_BUFFER,
                            _size, size,
                            (GL_MAP_WRITE_BIT_EXT           |
                             GL_MAP_UNSYNCHRONIZED_BIT_EXT  |
                             GL_MAP_INVALIDATE_RANGE_BIT_EXT));

        if (mapped) {
            memcpy(mapped, data, size);
            glUnmapBufferOES(GL_ARRAY_BUFFER);
        } else {
            DBGWarning(@"Failed to map vertex buffer object (0x%X)", glGetError());
        }
        
		{_size += size;}
	}
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LGLVertexAttribute

@end


@implementation LGLVertexAttributeParser

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSDictionary *)parse:(NSString *)string
{
    NSString        *attributeString    = [string stringByDeletingSubstring:@" "];
    NSArray         *attributeStrings   = [attributeString componentsSeparatedByString:@","];

    NSMutableArray  *attributes         = [NSMutableArray new];

    GLsizeiptr stride   = 0;
    GLsizeiptr index    = 0;

    for (NSString *format in attributeStrings) {
        LGLVertexAttribute      *attribute  = [LGLVertexAttribute new];

        NSError                 *error      = nil;

        NSRegularExpression     *scanner    =
        [NSRegularExpression
         regularExpressionWithPattern:
         @"(\\w+):"
         @"(p[234][sif]n?"
         @"|c[1234][bBsSiIf]n?"
         @"|e1[bBf]n?"
         @"|n3[bsif]n?"
         @"|t[234][sif]n?)"
         options:0
         error  :&error];

        NSTextCheckingResult    *match      =
        [scanner
         firstMatchInString :format
         options            :0
         range              :((NSRange) {
            0.,
            format.length
        })];

        if (!error) {
            if ([match numberOfRanges] > 2) {
                NSString *identifier    = [format substringWithRange:[match rangeAtIndex:1]];
                NSString *info          = [format substringWithRange:[match rangeAtIndex:2]];

                if (!error) {
                    error   = nil;
                    scanner =
                    [NSRegularExpression
                     regularExpressionWithPattern:
                     @"(\\w)(\\d)(\\w{1,2})"
                     options:0
                     error  :&error];

                    if (!error) {
                        match   =
                        [scanner
                         firstMatchInString :info
                         options            :0
                         range              :((NSRange) {
                            0.,
                            info.length
                        })];

                        NSString *lengthKey = [info substringWithRange:[match rangeAtIndex:2]];
                        NSString *typeKey   = [info substringWithRange:[match rangeAtIndex:3]];

                        [attribute setValuesForKeysWithDictionary:
                         (@{@"identifier"   : identifier,
                            @"index"        : @(index),
                            @"size"         : [self sizeForKey:typeKey],
                            @"type"         : [self typeForKey:typeKey],
                            @"length"       : [self lengthForKey:lengthKey],
                            @"normalized"   : [self normalizedForKey:typeKey]
                            })];
                    } else {
                        DBGWarning(@"Failed to parse info for attribute '%@' (\n%@\n)", identifier, [error localizedDescription]);

                        return nil;
                    }
                } else {
                    DBGWarning(@"Failed to parse info for attribute '%@' (missing fields)", identifier);
                    
                    return nil;
                }
            }
        } else {
            DBGWarning(@"Failed to parse attribute string '%@' (\n%@\n)", format, [error localizedDescription]);

            return nil;
        }

        [attributes addObject:attribute];

        index   += attribute->length * attribute->size;
        stride   = index;
    }
    
    return @{ @"attributes": attributes, @"stride": @(stride) };
}

+ (NSNumber *)sizeForKey:(NSString *)key
{
    static NSDictionary     *mapping    = nil;
    static dispatch_once_t  predicate   = 0;

    dispatch_once(&predicate, ^{
        mapping =
        (@{@GL_BYTE             : @(sizeof(GLbyte)),
            @GL_UNSIGNED_BYTE   : @(sizeof(GLubyte)),
            @GL_SHORT           : @(sizeof(GLshort)),
            @GL_UNSIGNED_SHORT  : @(sizeof(GLushort)),
            @GL_INT             : @(sizeof(GLint)),
            @GL_UNSIGNED_INT    : @(sizeof(GLuint)),
            @GL_FLOAT           : @(sizeof(GLfloat))
            });
    });

    return mapping[[self typeForKey:key]];
}

+ (NSNumber *)typeForKey:(NSString *)key
{
    static NSDictionary     *mapping    = nil;
    static dispatch_once_t  predicate   = 0;

    dispatch_once(&predicate, ^{
        mapping =
        (@{@'b': @GL_BYTE,
            @'B': @GL_UNSIGNED_BYTE,
            @'s': @GL_SHORT,
            @'S': @GL_UNSIGNED_SHORT,
            @'i': @GL_INT,
            @'I': @GL_UNSIGNED_INT,
            @'f': @GL_FLOAT
            });
    });

    return mapping[@([key characterAtIndex:0])];
}

+ (NSNumber *)lengthForKey:(NSString *)key
{
    return @([key integerValue]);
}

+ (NSNumber *)normalizedForKey:(NSString *)key
{
    return @([key length] > 1);
}

@end