//
//  LGLRenderer.h
//  Writeability
//
//  Created by Ryan on 12/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLVertexBuffer.h"
#import "LGLRenderable.h"



@class LGLProgram;
@class LGLTexture;



@interface LGLQuad : LGLVertexBuffer <LGLRenderable>

@property (nonatomic, readwrite , strong) LGLProgram                *program;
@property (nonatomic, readwrite , assign) LGLMatrix4x4              transform;
@property (nonatomic, readwrite , strong) UIColor                   *color;

@property (nonatomic, readonly  , strong) LGLTexture                *texture;

@property (nonatomic, readwrite , assign) CGRect                    region;
@property (nonatomic, readwrite , assign) CGRect                    target;

@property (nonatomic, readwrite , assign, getter = isVectorized)    BOOL vectorized;

@property (nonatomic, readonly  , assign, getter = isInverted)      BOOL inverted;


- (instancetype)initWithTexture:(LGLTexture *)texture;
- (instancetype)initWithTexture:(LGLTexture *)texture region:(CGRect)region;
- (instancetype)initWithTexture:(LGLTexture *)texture inverted:(BOOL)inverted;

- (instancetype)
initWithTexture :(LGLTexture *  )texture
region          :(CGRect        )region
vectorized      :(BOOL          )vectorized;

- (instancetype)
initWithTexture :(LGLTexture *  )texture
region          :(CGRect        )region
inverted        :(BOOL          )inverted;

- (instancetype)
initWithTexture :(LGLTexture *  )texture
region          :(CGRect        )region
vectorized      :(BOOL          )vectorized
inverted        :(BOOL          )inverted;

@end
