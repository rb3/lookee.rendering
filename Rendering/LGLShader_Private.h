//
//  LGLShader_Private.h
//  Writeability
//
//  Created by Ryan on 1/7/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLShader.h"


@interface LGLShader ()

@property (nonatomic, readonly, assign) int handle;

@end
