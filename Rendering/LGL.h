//
//  LGL.h
//  Writeability
//
//  Created by Ryan on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <OpenGLES/EAGLDrawable.h>

#import "LGLUtilities.h"


@interface LGL : NSObject

FOUNDATION_EXPORT const NSUInteger kLGLSurfaceCanvasDimension;


+ (NSMutableDictionary *)objectPool;

+ (GLboolean)setDrawable:(id <EAGLDrawable>)drawable;

+ (LGLIMatrix2x2)viewport;
+ (GLvoid)setViewport:(LGLIMatrix2x2)viewport;
+ (GLvoid)setViewWidth:(GLuint)width height:(GLuint)height;

+ (GLfloat)scale;
+ (GLvoid)setScale:(GLfloat)scale;

+ (GLboolean)blendingEnabled;
+ (GLvoid)setBlendingEnabled:(GLboolean)enabled;

+ (UIColor *)clearColor;
+ (GLvoid)setClearColor:(UIColor *)color;

+ (GLuint)program;
+ (GLvoid)setProgram:(GLuint)object;
+ (GLvoid)deleteProgram:(GLuint)object;

+ (GLuint)vertexArray;
+ (GLvoid)bindVertexArray:(GLuint)object;
+ (GLvoid)unbindVertexArray;
+ (GLvoid)deleteVertexArray:(GLuint)object;

+ (GLuint)vertexBufferForTarget:(GLenum)target;
+ (GLvoid)bindVertexBuffer:(GLuint)object toTarget:(GLenum)target;
+ (GLvoid)deleteVertexBuffer:(GLuint)object;

+ (GLuint)textureForTarget:(GLenum)target;
+ (GLvoid)activateTextureUnit:(GLenum)object;
+ (GLvoid)bindTexture:(GLuint)object toTarget:(GLenum)target;
+ (GLvoid)unbindTextureAtTarget:(GLenum)target;
+ (GLvoid)deleteTexture:(GLuint)object;

+ (GLuint)renderBufferForTarget:(GLenum)target;
+ (GLvoid)bindRenderBuffer:(GLuint)object toTarget:(GLenum)target;
+ (GLvoid)presentRenderBuffer:(GLuint)object;
+ (GLvoid)deleteRenderBuffer:(GLuint)object;

+ (GLuint)frameBufferForTarget:(GLenum)target;
+ (GLvoid)bindFrameBuffer:(GLuint)object toTarget:(GLenum)target;
+ (GLvoid)deleteFrameBuffer:(GLuint)object;

+ (void)queueAsyncOperation:(void(^)(void))block;
+ (void)queueSyncOperation:(void(^)(void))block;
+ (void)cancelPendingOperations;

@end
