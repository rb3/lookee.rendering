//
//  LGL.m
//  Writeability
//
//  Created by Ryan on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGL.h"

#import "LGLProgram.h"

#import <Utilities/UIColor+Utilities.h>

#import <OpenGLES/ES2/glext.h>

#import <map>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGL
#pragma mark -
//*********************************************************************************************************************//




@implementation LGL

#pragma mark - External Objects
//*********************************************************************************************************************//

const NSUInteger kLGLSurfaceCanvasDimension = 792;


#pragma mark - Static Objects
//*********************************************************************************************************************//

static LGLIMatrix2x2    vLGLViewport            = {0};
static GLfloat          vLGLScale               = 1.;

static BOOL             vLGLBlendingEnabled		= NO;
static BOOL             vLGLBlendFuncEnabled	= NO;

static GLuint           vLGLProgram             = 0;
static GLuint           vLGLVertexArray         = 0;
static GLenum           vLGLTextureUnit         = GL_TEXTURE0;

static UIColor          *vLGLClearColor         = nil;

/**
 * These map OpenGL constants to the appropriate OpenGL objects.
 */
static std::map<GLenum, GLuint> vLGLVertexBuffers;
static std::map<GLenum, GLuint> vLGLTextures;
static std::map<GLenum, GLuint> vLGLRenderBuffers;
static std::map<GLenum, GLuint> vLGLFrameBuffers;


#pragma mark - Loader
//*********************************************************************************************************************//

+ (GLvoid)load
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector   :@selector(pauseRender:)
     name       :UIApplicationWillResignActiveNotification
     object     :nil];

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector   :@selector(resumeRender:)
     name       :UIApplicationDidBecomeActiveNotification
     object     :nil];
}


#pragma mark - Singletons
//*********************************************************************************************************************//

+ (EAGLContext *)mainContext
{
	static dispatch_once_t	predicate	= 0;
	static EAGLContext		*context	= nil;

    dispatch_once(&predicate, ^{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
        dispatch_block_t job = ^{
#endif
            context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
        };

        if ([NSOperationQueue currentQueue] == [NSOperationQueue mainQueue]) {
            job();
        } else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:job];
            [[NSOperationQueue mainQueue] waitUntilAllOperationsAreFinished];
        }
#endif
    });

    return context;
}

+ (EAGLContext *)operationContext
{
	static dispatch_once_t	predicate	= 0;
	static EAGLContext		*context	= nil;

    dispatch_once(&predicate, ^{
        dispatch_block_t job = ^{
            context = [[EAGLContext alloc]
                       initWithAPI  :self.mainContext.API
                       sharegroup   :self.mainContext.sharegroup];
        };

        if ([NSOperationQueue currentQueue] == [NSOperationQueue mainQueue]) {
            job();
        } else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:job];
            [[NSOperationQueue mainQueue] waitUntilAllOperationsAreFinished];
        }
    });

    return context;
}

+ (NSOperationQueue *)operationQueue
{
    static dispatch_once_t	predicate	= 0;
	static NSOperationQueue *queue      = nil;

    dispatch_once(&predicate, ^{
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    });

    return queue;
}

+ (NSMutableDictionary *)objectPool
{
	static dispatch_once_t      predicate   = 0;
	static NSMutableDictionary	*pool       = nil;

    dispatch_once(&predicate, ^{
        pool = [NSMutableDictionary new];
    });

    return pool;
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

// !!!: Context
+ (GLboolean)setDrawable:(id <EAGLDrawable>)drawable
{
    return [
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
            [self operationContext]
#else
            [self mainContext]
#endif
            renderbufferStorage :GL_RENDERBUFFER
            fromDrawable        :drawable
            ];
}

// !!!: Viewport
+ (LGLIMatrix2x2)viewport
{
    return vLGLViewport;
}

+ (GLvoid)setViewport:(LGLIMatrix2x2)viewport
{
    if (!LGLIMatrix2x2AllEqualToIMatrix2x2(vLGLViewport, viewport)) {
        {vLGLViewport = viewport;}

        glViewport(viewport.x, viewport.y, viewport.w, viewport.h);
    }
}

+ (GLvoid)setViewWidth:(GLuint)width height:(GLuint)height
{
    [self setViewport:((LGLIMatrix2x2){0, 0, static_cast<int>(width), static_cast<int>(height)})];
}

// !!!: Scale
+ (GLfloat)scale
{
    return vLGLScale;
}

+ (GLvoid)setScale:(GLfloat)scale
{
    vLGLScale = scale;
}

// !!!: Blending
+ (GLboolean)blendingEnabled
{
	return vLGLBlendingEnabled;
}

+ (GLvoid)setBlendingEnabled:(GLboolean)enabled
{
	if (vLGLBlendingEnabled != enabled) {
		{vLGLBlendingEnabled = enabled;}
        
		if (vLGLBlendingEnabled) {
			glEnable(GL_BLEND);

			if (vLGLBlendFuncEnabled == NO) {
				{vLGLBlendFuncEnabled = YES;}

				glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			}
		} else {
			glDisable(GL_BLEND);
		}
	}
}

// !!!: Color
+ (UIColor *)clearColor
{
    return vLGLClearColor;
}

+ (GLvoid)setClearColor:(UIColor *)color
{
    if ((vLGLClearColor != color) && ![vLGLClearColor isEqualToColor:color]) {
        {vLGLClearColor = color;}

        if (vLGLClearColor) {
            [vLGLClearColor bindGLClearColor];
        } else {
            [[UIColor blackColor] bindGLClearColor];
        }
    }
}

// !!!: Program
+ (GLuint)program
{
    return vLGLProgram;
}

+ (GLvoid)setProgram:(GLuint)object
{
    if (vLGLProgram != object) {
        glUseProgram(object);

        {vLGLProgram = object;}
    }
}

+ (GLvoid)deleteProgram:(GLuint)object
{
    if (object) {
        if (vLGLProgram == object) {
            vLGLProgram = 0;
        }

        glDeleteProgram(object);
    }
}


// !!!: Vertex array
+ (GLuint)vertexArray
{
    return vLGLVertexArray;
}

+ (GLvoid)bindVertexArray:(GLuint)object
{
    if (vLGLVertexArray != object) {
        glBindVertexArrayOES(object);

        {vLGLVertexArray = object;}
    }
}

+ (GLvoid)unbindVertexArray
{
    if (vLGLVertexArray != 0) {
        {vLGLVertexArray = 0;}

        glBindVertexArrayOES(0);
    }
}

+ (GLvoid)deleteVertexArray:(GLuint)object
{
    if (object) {
        if (vLGLVertexArray == object) {
            vLGLVertexArray = 0;
        }

        glDeleteVertexArraysOES(1, &object);
    }
}

// !!!: Vertex buffer
+ (GLuint)vertexBufferForTarget:(GLenum)target
{
    return vLGLVertexBuffers.count(target)? vLGLVertexBuffers[target]: 0;
}

+ (GLvoid)bindVertexBuffer:(GLuint)object toTarget:(GLenum)target
{
    GLuint bound = vLGLVertexBuffers[target];

    if (bound != object) {
        glBindBuffer(target, object);

        vLGLVertexBuffers[target] = object;
    }
}

+ (GLvoid)deleteVertexBuffer:(GLuint)object
{
    if (object) {
        for (auto item = vLGLVertexBuffers.begin(); item != vLGLVertexBuffers.end();) {
            if (item->second == object) {
                vLGLVertexBuffers.erase(item ++);
            } else {
                ++ item;
            }
        }

        glDeleteBuffers(1, &object);
    }
}

// !!!: Texture
+ (GLuint)textureForTarget:(GLenum)target
{
    return vLGLTextures.count(target)? vLGLTextures[target]: 0;
}

+ (GLvoid)activateTextureUnit:(GLenum)object
{
    if (vLGLTextureUnit != object) {
        {vLGLTextureUnit = object;}

        glActiveTexture(object);
    }
}

+ (GLvoid)bindTexture:(GLuint)object toTarget:(GLenum)target
{
    GLuint bound = vLGLTextures[target];

    if (bound != object) {
        glBindTexture(target, object);

        vLGLTextures[target] = object;
    }
}

+ (GLvoid)unbindTextureAtTarget:(GLenum)target
{
    GLuint bound = vLGLTextures[target];

    if (bound) {
        glBindTexture(target, 0);

        vLGLTextures.erase(target);
    }
}

+ (GLvoid)deleteTexture:(GLuint)object
{
    if (object) {
        for (auto item = vLGLTextures.begin(); item != vLGLTextures.end();) {
            if (item->second == object) {
                vLGLTextures.erase(item ++);
            } else {
                ++ item;
            }
        }

        glDeleteTextures(1, &object);
    }
}

// !!!: Render buffer
+ (GLuint)renderBufferForTarget:(GLenum)target
{
    return vLGLRenderBuffers.count(target)? vLGLRenderBuffers[target]: 0;
}

+ (GLvoid)bindRenderBuffer:(GLuint)object toTarget:(GLenum)target
{
    GLuint bound = vLGLRenderBuffers[target];

    if (bound != object) {
        glBindRenderbuffer(target, object);

        vLGLRenderBuffers[target] = object;
    }
}

+ (GLvoid)presentRenderBuffer:(GLuint)object
{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    dispatch_block_t job = ^{
#endif
        if ([EAGLContext setCurrentContext:self.mainContext]) {
            [self bindRenderBuffer:object toTarget:GL_RENDERBUFFER];
            [self.mainContext presentRenderbuffer:GL_RENDERBUFFER];
        } else {
            DBGWarning(@"Failed to set current OpenGL context (0x%X)", glGetError());
        }
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    };

    [self queueSyncOperation:^{ glFlush(); }];

    if ([NSOperationQueue currentQueue] == [NSOperationQueue mainQueue]) {
        job();
    } else {
        [[NSOperationQueue mainQueue] addOperationWithBlock:job];
    }
#endif
}

+ (GLvoid)deleteRenderBuffer:(GLuint)object
{
    if (object) {
        for (auto item = vLGLRenderBuffers.begin(); item != vLGLRenderBuffers.end();) {
            if (item->second == object) {
                vLGLRenderBuffers.erase(item ++);
            } else {
                ++ item;
            }
        }

        glDeleteRenderbuffers(1, &object);
    }
}

// !!!: Frame buffer
+ (GLuint)frameBufferForTarget:(GLenum)target
{
    return vLGLFrameBuffers.count(target)? vLGLFrameBuffers[target]: 0;
}

+ (GLvoid)bindFrameBuffer:(GLuint)object toTarget:(GLenum)target
{
    GLuint bound = vLGLFrameBuffers[target];

    if (bound != object) {
        vLGLFrameBuffers[target] = object;

        glBindFramebuffer(target, object);
    }
}

+ (GLvoid)deleteFrameBuffer:(GLuint)object
{
    if (object) {
        for (auto item = vLGLFrameBuffers.begin(); item != vLGLFrameBuffers.end();) {
            if (item->second == object) {
                vLGLFrameBuffers.erase(item ++);
            } else {
                ++ item;
            }
        }

        glDeleteFramebuffers(1, &object);
    }
}

// !!!: Job queue
+ (void)queueSyncOperation:(void(^)(void))block
{
    [self queueAsyncOperation:block];
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    if ([NSOperationQueue currentQueue] != [self operationQueue]) {
        [self.operationQueue waitUntilAllOperationsAreFinished];
    }
#endif
}

+ (void)queueAsyncOperation:(void(^)(void))block
{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
	dispatch_block_t job = ^{
        if ([EAGLContext setCurrentContext:self.operationContext]) {
            block();

            if ([self.operationQueue isSuspended]) {
                glFinish();
            }

            if ([self.operationQueue isSuspended]) {
                glFinish();
            }

            if ([self.operationQueue isSuspended]) {
                glFinish();
            }
        } else {
            DBGWarning(@"Failed to set current OpenGL context (0x%X)", glGetError());
        }
    };

    if ([NSOperationQueue currentQueue] == [self operationQueue]) {
        job();
    } else {
        [self.operationQueue addOperationWithBlock:job];
    }
#else
    if ([EAGLContext setCurrentContext:self.mainContext]) {
        block();
    } else {
        DBGWarning(@"Failed to set current OpenGL context (0x%X)", glGetError());
    }
#endif
}

+ (void)cancelPendingOperations
{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    if ([NSOperationQueue currentQueue] != [self operationQueue]) {
        [self.operationQueue cancelAllOperations];
    }
#endif
}


#pragma mark - NSNotificationCenter Callbacks
//*********************************************************************************************************************//

+ (void)pauseRender:(NSNotification *)notification
{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    if (![self.operationQueue isSuspended]) {
        DBGInformation(@"Rendering will be paused.");

        [self.operationQueue setSuspended:YES];

        glFinish();
    }
#else
    glFinish();
#endif
}

+ (void)resumeRender:(NSNotification *)notification
{
#if !DEBUG_DISABLE_BACKGROUND_RENDERING
    if ([self.operationQueue isSuspended]) {
        [self.operationQueue setSuspended:NO];

        DBGInformation(@"Rendering was resumed.");
    }
#endif
}

@end
