//
//  LGLAtlas.m
//  Writeability
//
//  Created by Ryan on 1/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LGLAtlas.h"

#import <UIKit/UIGraphics.h>

#import <vector>


// TODO: Change sprite coordinate system to the Core Graphics coordinate system




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLAtlas
#pragma mark -
//*********************************************************************************************************************//




@implementation LGLAtlas

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    std::vector<LGLIMatrix2x2> _nodes;

    BOOL __cleared;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
data            :(NSData *  )data
{
    self = [super
            initWithWidth   :width
            height          :height
            depth           :depth
            scale           :scale
            data            :data];

    if (self) {
        LGLIMatrix2x2 spacer = {
            .x = 1,
            .y = 1,
            .w = static_cast<int>([self columns]-2),
            .h = static_cast<int>([self rows]-2)
        };

        _nodes.push_back(spacer);
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)clear
{
    [super clear];

    _nodes.clear();
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (CGRect)
addBitmap   :(NSData *  )bitmap
withRows    :(NSUInteger)rows
stride      :(NSUInteger)stride
{
    NSUInteger      columns = DivF3([bitmap length], [self depth], rows);
    
    LGLIMatrix2x2   region  = [self regionWithWidth:columns+1 height:rows+1];

    if ((region.w > 0) && (region.h > 0)) {
        CGRect rect = CGRectScale(((CGRect) {
            .origin = {
                .x = static_cast<CGFloat>(region.x),
                .y = static_cast<CGFloat>(region.y)
            },
            .size   = {
                .width  = static_cast<CGFloat>(region.w),
                .height = static_cast<CGFloat>(region.h)
            }
        }), LFMRecf([self scale]));

        if (__cleared != YES) {
            {__cleared = YES;}

            [self clear];
        }

        [self
         setBitmap  :bitmap
         withRows   :rows
         stride     :stride
         atRow      :region.y
         column     :region.x];

        return rect;
    }

    return CGRectNull;
}

- (CGRect)
renderSprite:(void(^)(void) )block
withWidth   :(NSUInteger    )width
height      :(NSUInteger    )height
{
    CGFloat         scale   = [self scale];

    NSUInteger      columns = LFMCeilf(width  * scale);
    NSUInteger      rows    = LFMCeilf(height * scale);

    LGLIMatrix2x2   region  = [self regionWithWidth:columns+1 height:rows+1];

    if ((region.w > 0) && (region.h > 0)) {
        CGFloat xScale  = LFMDivf(width, columns);
        CGFloat yScale  = LFMDivf(height, rows);

        CGRect  rect    = ((CGRect) {
            .origin = {
                .x = (region.x * xScale),
                .y = (region.y * yScale)
            },
            .size   = {
                .width  = static_cast<CGFloat>(width),
                .height = static_cast<CGFloat>(height)
            }
        });

        if (__cleared != YES) {
            {__cleared = YES;}

            [self clear];
        }

        [self drawBitmap:^{
            CGContextRef context = UIGraphicsGetCurrentContext();

            CGContextTranslateCTM(context, 0., self.height);
            CGContextScaleCTM(context, 1., -1.);

            CGContextClipToRect(context, rect);
            CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);

            block();
        }];

        return rect;
    }

    return CGRectNull;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LGLIMatrix2x2)regionWithWidth:(NSUInteger)width height:(NSUInteger)height
{
    LGLIMatrix2x2   region      = {{0, 0, static_cast<int>(width), static_cast<int>(height)}};

    NSInteger       bestWidth   = NSIntegerMax;
    NSInteger       bestHeight  = NSIntegerMax;

    NSInteger       nodeIndex   = NSNotFound;

	for (auto node = _nodes.begin(); node != _nodes.end(); ++ node) {
        NSInteger mantissa = NSNotFound;

        if (node->x + width < [self columns]) {
            auto        next        = node;
            NSInteger   remainder   = width;

            mantissa = node->y;

            while (remainder > 0) {
                if (mantissa <= next->y) {
                    mantissa = next->y;
                }

                if (mantissa + height >= [self rows]) {
                    mantissa = NSNotFound;
                    break;
                }

                remainder -= next ++ ->w;
            }
        }

		if (mantissa != NSNotFound) {
			if((mantissa + height < bestHeight) ||
               (mantissa + height == bestHeight &&
                node->w < bestWidth))
            {
                bestWidth   = node->w;
				bestHeight  = mantissa + height;

				nodeIndex   = std::distance(_nodes.begin(), node);

				region.x    = node->x;
				region.y    = mantissa;
			}
        }
    }

	if (nodeIndex != NSNotFound) {
        _nodes.insert
        (_nodes.begin() + nodeIndex,
         ((LGLIMatrix2x2) {
            .x = region.x,
            .y = region.y + static_cast<int>(height),
            .w = static_cast<int>(width),
            .h = static_cast<int>(height)
        }));

        for (NSUInteger index = nodeIndex +1; index < _nodes.size(); ++ index) {
            LGLIMatrix2x2 &node           = _nodes[index -0];
            LGLIMatrix2x2 const &previous = _nodes[index -1];

            if (node.x < (previous.x + previous.w)) {
                NSInteger shrink = previous.x + previous.w - node.x;

                if ((node.w -= shrink) <= 0) {
                    _nodes.erase(_nodes.begin() + index --);
                } else {
                    node.x += shrink;
                    break;
                }
            } else {
                break;
            }
        }

        for (NSUInteger index = 0; index < _nodes.size() -1; ++ index) {
            LGLIMatrix2x2 &node       = _nodes[index +0];
            LGLIMatrix2x2 const &next = _nodes[index +1];

            if (node.y == next.y) {
                node.w += next.w;

                _nodes.erase(_nodes.begin() + index -- +1);
            }
        }
    } else {
        region.x = 0;
        region.y = 0;
        region.w = 0;
        region.h = 0;
    }

    return region;
}

@end
