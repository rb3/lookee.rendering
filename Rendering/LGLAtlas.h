//
//  LGLAtlas.h
//  Writeability
//
//  Created by Ryan on 1/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import "LGLTexture.h"



@interface LGLAtlas : LGLTexture

- (CGRect)
addBitmap   :(NSData *  )bitmap
withRows    :(NSUInteger)rows
stride      :(NSUInteger)stride;

- (CGRect)
renderSprite:(void(^)(void) )block
withWidth   :(NSUInteger    )width
height      :(NSUInteger    )height;

@end
